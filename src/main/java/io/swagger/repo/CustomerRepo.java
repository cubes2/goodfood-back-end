package io.swagger.repo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import io.swagger.myMongoDB;
import io.swagger.Utils.ItemToDocument;
import io.swagger.buisness.Interfaces.ABaseRepo;
import io.swagger.model.Customer;

public class CustomerRepo extends ABaseRepo<Customer> {

	public CustomerRepo() {
		super("Customers");
		itemList = new ArrayList<Customer>();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Customer> GetAll() {
		System.out.println("CustomerApiImpl::getAllCustomers myMongoDB created");
    	MongoCollection<Document> customers = db.GetDocumentBySellection("Customers");
    	System.out.println("CustomerApiImpl::getAllCustomers MongoCollection<Customer> recovered");
    	MongoCursor<Document> customersCursor = customers.find().iterator(); 
    	System.out.println("CustomerApiImpl::getAllCustomers MongoCursor<Customer> init");
    	itemList.clear();
        try 
        { 
            while (customersCursor.hasNext()) 
            { 
            	Document currentDoc = customersCursor.next();
                System.out.println(currentDoc.toString());
                try {
                	
                	itemList.add(ItemToDocument.documentToCustomer(currentDoc));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("CustomerApiImpl::getAllCustomers all.add documentToCustomer error");
				}
            } 
        }
        finally 
        { 
        	customersCursor.close(); 
        } 
		return itemList;
	}

	@Override
	public Customer GetById(ObjectId id) {
		Customer customerToReturn = null;
		System.out.println("customerService::GetById serched id is = " + id.toString());
		List<Customer> allCustomers = new ArrayList<Customer>();
		allCustomers = GetAll();
		System.out.println("customerService::GetById allCustomers.size() = " + allCustomers.size());
		for(Customer customer : allCustomers)
		{
			System.out.println("customerService::GetById customer.getId() = " + customer.getId().toString());
			if(customer.getId().equals(id))
			{
				customerToReturn = customer;
			}
		}
		if(customerToReturn != null)
		{
			System.out.println("customerService::GetById customerToReturn = " + customerToReturn.toString());
		}
		return customerToReturn;
	}

	@Override
	public boolean Update(ObjectId id, Customer object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Create(Customer object) {
		boolean success = false;
		if(db.CreateModel("Customers",object.toDocument()))
		{
			success = true;
		}
		return success;
	}

	@Override
	public boolean Delete(ObjectId id) {
		// TODO Auto-generated method stub
		return false;
	}

}

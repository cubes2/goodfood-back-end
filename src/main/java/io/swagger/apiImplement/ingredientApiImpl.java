package io.swagger.apiImplement;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import io.swagger.myMongoDB;
import io.swagger.api.IngredientApi;
import io.swagger.buisness.services.impl.IngredientService;
import io.swagger.model.Ingredient;
import io.swagger.model.Recipe;
import io.swagger.model.Restaurant;

public class ingredientApiImpl implements IngredientApi {

    
	public ingredientApiImpl() {
		mIngredientService = new IngredientService();
	}

	@Override
	public ResponseEntity<Void> createIngredient(@Valid Document body) {
		
		System.out.println("ingredientApiImpl::createIngredient");
		if(mIngredientService.Create(body))
		{
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<Void> deleteIngredient(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Recipe> getAllIngredientRecipes(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Ingredient>> getAllIngredients() 
	{
		System.out.println("ingredientApiImpl::getAllIngredients entering");
		List<Ingredient> all = new ArrayList<Ingredient>();
		
    	all = mIngredientService.GetAll();
        System.out.println("ingredientApiImpl::getAllIngredients return");
    	return new ResponseEntity<List<Ingredient>>(all, HttpStatus.OK);
    }

	@Override
	public ResponseEntity<Ingredient> getIngredient(String uid) {
		return new ResponseEntity<Ingredient>(mIngredientService.GetById(new ObjectId(uid)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Void> updateIngredient(String uid, @Valid Document body) {
		// TODO Auto-generated method stub
		return null;
	}
	private IngredientService mIngredientService;

}

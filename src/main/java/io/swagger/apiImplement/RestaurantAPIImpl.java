package io.swagger.apiImplement;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import io.swagger.api.RestaurantApi;
import io.swagger.buisness.services.impl.RestaurantService;
import io.swagger.model.Customer;
import io.swagger.model.Discount;
import io.swagger.model.Feedback;
import io.swagger.model.Order;
import io.swagger.model.Purchase;
import io.swagger.model.Recipe;
import io.swagger.model.Restaurant;
import io.swagger.model.Supplier;

public class RestaurantAPIImpl implements RestaurantApi {

	public RestaurantAPIImpl() {
		mRestaurantService = new RestaurantService();
	}

	@Override
	public ResponseEntity<Void> createRestaurant(Document body) {
		System.out.println("RestaurantAPIImpl::createRestaurant");
		if(mRestaurantService.Create(body))
		{
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<Void> deleteRestaurant(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Discount>> getAllRestaurantDiscounts(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Order> getAllRestaurantOrders(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Recipe>> getAllRestaurantRecipes(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Supplier>> getAllRestaurantSuppliers(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Restaurant>> getAllRestaurants() {
		System.out.println("RestaurantApiImpl::getAllRestaurants entering");
		List<Restaurant> all = new ArrayList<Restaurant>();
		
    	all = mRestaurantService.GetAll();
        System.out.println("RestaurantApiImpl::getAllRestaurants return");
    	return new ResponseEntity<List<Restaurant>>(all, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Restaurant> getRestaurant(String uid) {
		return new ResponseEntity<Restaurant>(mRestaurantService.GetById(new ObjectId(uid)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Restaurant>> getRestaurantByGps(String gps) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Feedback> getRestaurantFeedbacks(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Purchase> getRestaurantPurchases(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> updateRestaurant(String uid, Document body) {
		mRestaurantService.Update(new ObjectId(uid), body);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	private RestaurantService mRestaurantService;

}

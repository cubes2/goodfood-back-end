package io.swagger.apiImplement;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import io.swagger.api.RecipeApi;
import io.swagger.buisness.services.impl.RecipeService;
import io.swagger.model.Discount;
import io.swagger.model.Feedback;
import io.swagger.model.Ingredient;
import io.swagger.model.Recipe;
import io.swagger.model.Restaurant;

public class RecipeApiImpl implements RecipeApi {

	public RecipeApiImpl() {
		mRecipeService = new RecipeService();
	}

	@Override
	public ResponseEntity<Void> addRecipe(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> createRecipe(Document body) {
		System.out.println("RecipeApiImpl::createRecipe");
		if(mRecipeService.Create(body))
		{
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<Void> deleteRecipe(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Discount>> getAllRecipeActiveDiscounts(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Ingredient> getAllRecipeIngredients(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Restaurant> getAllRecipeRestaurants(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Recipe>> getAllRecipes() {
		System.out.println("RecipeApiImpl::getAllRecipes entering");
		List<Recipe> all = new ArrayList<Recipe>();
		
    	all = mRecipeService.GetAll();
        System.out.println("RecipeApiImpl::getAllRecipes return");
    	return new ResponseEntity<List<Recipe>>(all, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Recipe> getRecipe(String uid) {
		return new ResponseEntity<Recipe>(mRecipeService.GetById(new ObjectId(uid)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Feedback> getRecipeFeedbacks(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> removeRecipe(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> standardiseRecipe(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> updateRecipe(String uid, Document body) {
		mRecipeService.Update(new ObjectId(uid), body);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	private RecipeService mRecipeService;

}

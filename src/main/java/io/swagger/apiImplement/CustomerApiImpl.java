package io.swagger.apiImplement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.threeten.bp.OffsetDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import io.swagger.myMongoDB;
import io.swagger.api.CustomerApi;
import io.swagger.buisness.services.impl.CustomerService;
import io.swagger.model.Address;
import io.swagger.model.Complaint;
import io.swagger.model.Customer;
import io.swagger.model.CustomerInformation;
import io.swagger.model.CustomerRGPD;
import io.swagger.model.Feedback;
import io.swagger.model.Order;
import io.swagger.repo.CustomerRepo;

public class CustomerApiImpl implements CustomerApi {

	public CustomerApiImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ResponseEntity<Void> createComplaint(String uid, Complaint body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> createCustomer(Document body) {
		// TODO Auto-generated method stub
		if(mCustomerService.Create(body))
		{
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
			
		
	}

	@Override
	public ResponseEntity<Void> createCustomerAddress(String uid, Address body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> createCustomerInformation(String uid, CustomerInformation body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> createCustomerOrder(String uid, Order body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> createCustomerRGPD(String uid, CustomerRGPD body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> createFeedback(String uid, Feedback body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> deleteCustomer(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> deleteCustomerInformation(String uid) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public ResponseEntity<List<Customer>> getAllCustomers() {
		System.out.println("CustomerApiImpl::getAllCustomers entering");
		List<Customer> all = new ArrayList<Customer>();
		
    	all = mCustomerService.GetAll();
        System.out.println("CustomerApiImpl::getAllCustomers return");
    	return new ResponseEntity<List<Customer>>(all, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Customer> getCustomer(String uid) {
		
		return new ResponseEntity<Customer>(mCustomerService.GetById(new ObjectId(uid)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Address> getCustomerAddresses(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Complaint> getCustomerComplaints(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Feedback> getCustomerFeedbacks(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<CustomerInformation> getCustomerInformation(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Order> getCustomerOrders(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<CustomerRGPD> getCustomerRGPD(String uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> updateCustomer(String uid, Customer body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> updateCustomerInformation(String uid, CustomerInformation body) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> updateCustomerRGPD(String uid, CustomerRGPD body) {
		// TODO Auto-generated method stub
		return null;
	}
	private CustomerService mCustomerService = new CustomerService(new CustomerRepo());
	

}

package io.swagger.configuration;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.buisness.services.impl.TokenAuthenticationService;

public class logoutSucessHandler extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {
	private static final String REDIS_SET_ACTIVE_SUBJECTS = "active-subjects";
	  static final String SECRET = "ThisIsASecret";
	    
	    static final String TOKEN_PREFIX = "Bearer";
	    
	    static final String HEADER_STRING = "Authorization";
	public logoutSucessHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

    @Override
    public void onLogoutSuccess(
      HttpServletRequest request, 
      HttpServletResponse response, 
      Authentication authentication) 
      throws IOException, ServletException {
    	String username = request.getParameter("username");
    	System.out.println("logoutSucessHandler::onLogoutSuccess username = " + username);
    	System.out.println("logoutSucessHandler::onLogoutSuccess authentication = " + authentication);
    	
    	authentication = TokenAuthenticationService.getAuthentication(request);
    	System.out.println("logoutSucessHandler::onLogoutSuccess authentication = " + authentication);
    	System.out.println("logoutSucessHandler::onLogoutSuccess SecurityContextHolder.getContext().getAuthentication(); = " + SecurityContextHolder.getContext().getAuthentication());
    	
    	authentication.setAuthenticated(false);
    	SecurityContextHolder.getContext().setAuthentication(authentication);
    	String JWT = Jwts.builder().setSubject(request.getParameter("username"))
                .setExpiration(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();
    	response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
        
        request.logout();
    }

}

package io.swagger.configuration;

import io.swagger.buisness.services.impl.CustomerService;
import io.swagger.filter.JWTAuthenticationFilter;
import io.swagger.filter.JWTLoginFilter;
import io.swagger.model.Customer;
import io.swagger.repo.CustomerRepo;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public LogoutSuccessHandler logoutSuccessHandler() {
	    return new logoutSucessHandler();
	}
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                // No need authentication.
                .antMatchers("/").permitAll() //
                .antMatchers(HttpMethod.POST, "/login").permitAll() //
                .antMatchers(HttpMethod.GET, "/login").permitAll() // For Test on Browser
                // Need authentication.
                .anyRequest().authenticated()

                .and()
                .logout()
                .logoutUrl("/logout")                          
                .invalidateHttpSession(true)
                .logoutSuccessHandler(logoutSuccessHandler())
                .and()
                //
                // Add Filter 1 - JWTLoginFilter
                //
                .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
                        UsernamePasswordAuthenticationFilter.class)
                //
                // Add Filter 2 - JWTAuthenticationFilter
                //
                .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        http.cors();
    }
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "http://localhost:8082"));
        configuration.setAllowedMethods(Arrays.asList("GET", "PUT", "POST","OPTIONS", "DELETE"));
        configuration.setAllowedHeaders(Arrays.asList("authorization","content-type"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
    

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	CustomerService customerService = new CustomerService(new CustomerRepo());
    	List<Customer> customerList = customerService.GetAll();
    	for(Customer customer : customerList)
    	{

            String encrytedPassword = this.passwordEncoder().encode(customer.getPassword());
            System.out.println("Encoded password of 123=" + encrytedPassword);

            InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> //
            mngConfig = auth.inMemoryAuthentication();
            System.out.println("WebSecurityConfig::configure customer.tostring=" + customer.toString());

            // Defines 2 users, stored in memory.
            // ** Spring BOOT >= 2.x (Spring Security 5.x)
            // Spring auto add ROLE_
            UserDetails u1 = User.withUsername(customer.getEmail()).password(encrytedPassword).roles("USER").build();
            mngConfig.withUser(u1);
    	}
        

        // If Spring BOOT < 2.x (Spring Security 4.x)):
        // Spring auto add ROLE_
        // mngConfig.withUser("tom").password("123").roles("USER");
        // mngConfig.withUser("jerry").password("123").roles("USER");

    }
    
}
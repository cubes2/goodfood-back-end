package io.swagger;

import org.bson.Document;
import org.reflections.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import io.swagger.model.Customer;
import io.swagger.model.Ingredient;

public class myMongoDB {
	public myMongoDB()
	{
		String connectionString;
		try 
		{
			connectionString = System.getenv("CONNECTION_STRING");
			if(connectionString.isEmpty())
			{
				connectionString = "mongodb://127.0.0.1:27017";
			}
		} 
		catch (Exception e) 
		{
			connectionString = "mongodb://127.0.0.1:27017";
		}
		
		System.out.println("myMongoDB constructor entering connectionString = " + connectionString);
		_mongoDBClient = MongoClients.create(connectionString);
		System.out.println("myMongoDB constructor mongodb connection ok");
		_goodFoodDatabase = _mongoDBClient.getDatabase("goodFood");
		
		if(!_goodFoodDatabase.getName().isEmpty())
		{
			System.out.println("myMongoDB constructor mongodb getDatabase ok");
		}
		
		
	}
	private MongoClient _mongoDBClient;
	private MongoDatabase _goodFoodDatabase;
	public MongoCollection<Ingredient> GetIgredients()
	{
		System.out.println("myMongoDB::GetIgredients entering");
		MongoCollection<Ingredient> ingredients = _goodFoodDatabase.getCollection("Ingredients", Ingredient.class);
		return ingredients;
	}
	public MongoCollection<Document> GetDocumentBySellection(String collectionName)
	{
		return _goodFoodDatabase.getCollection(collectionName);
	}
	public MongoCollection<Customer> GetCustomers()
	{
		System.out.println("myMongoDB::GetIgredients entering");
		MongoCollection<Customer> customers = _goodFoodDatabase.getCollection("Customers", Customer.class);
		return customers;
	}
	public boolean CreateModel(String collectionName, Document objectToPutOnDb)
	{
		boolean sucessToReturn = false;
		MongoCollection<Document> selection = GetDocumentBySellection(collectionName);
		selection.insertOne(objectToPutOnDb);
		return true;
	}

}

package io.swagger.Utils;

import java.text.ParseException;
import java.util.Map;

import org.bson.Document;

import io.swagger.model.Customer;
import io.swagger.model.CustomerInformation;
import io.swagger.model.CustomerRGPD;

public class ItemToDocument {

	public static Customer documentToCustomer(Document doc) throws ParseException
	{
		System.out.println("ItemToDocument::documentToCustomer doc = " + doc.toString());
		Customer customerToReturn = new Customer();
		customerToReturn.setId(doc.getObjectId("_id"));
		customerToReturn.setEmail((String) doc.get("email"));
		customerToReturn.setPassword((String) doc.get("password"));
		//customer info
		System.out.println("ItemToDocument::documentToCustomer doc.get(\"information\") = " + doc.get("information").toString());
		Document customerInfosDoc = new Document((Map<String, Object>) doc.get("information"));
		CustomerInformation customerInfos = documentToCustomerInformation(customerInfosDoc);
		System.out.println("ItemToDocument::documentToCustomer doc = " + doc.toString());
		//customer rgpd
		Document customerRgpdDoc = new Document( (Map<String, Object>) doc.get("rgpd"));
		CustomerRGPD customerRgpd = new CustomerRGPD(customerRgpdDoc);
		customerToReturn.setInformation(customerInfos);
		customerToReturn.setRgpd(customerRgpd);
		System.out.println("ItemToDocument::documentToCustomer customerToReturn = " + customerToReturn.toString());
		return customerToReturn;
	}
	public static CustomerInformation documentToCustomerInformation(Document doc) throws ParseException
	{
		System.out.println("ingredientApiImpl::documentToCustomer doc = " + doc.toString());
		CustomerInformation CustomerInformationcustomerToReturn = new CustomerInformation(doc);
		return CustomerInformationcustomerToReturn;
	}
}

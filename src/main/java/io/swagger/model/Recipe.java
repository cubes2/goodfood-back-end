package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.CellProduct;
import io.swagger.buisness.types.ObjectIdSerializer2;
import io.swagger.buisness.types.StoreProduct;
import net.bytebuddy.asm.Advice.This;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * recipe model
 */
@ApiModel(description = "recipe model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Recipe   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("owner")
  private String owner = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("photo")
  @Valid
  private List<String> photo = new ArrayList<String>();

  @JsonProperty("standardPrice")
  private double standardPrice = 0.0;

  @JsonProperty("isStandard")
  private Boolean isStandard = null;

  @JsonProperty("ingredients")
  @Valid
  private List<StoreProduct> ingredients = new ArrayList<StoreProduct>();

  public Recipe id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the recipe id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the recipe id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Recipe owner(String owner) {
    this.owner = owner;
    return this;
  }

  /**
   * the id of the restaurant owning this recipe if the recipe is not part of the good food official recipes
   * @return owner
  **/
  @ApiModelProperty(value = "the id of the restaurant owning this recipe if the recipe is not part of the good food official recipes")


  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public Recipe name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the recipe name
   * @return name
  **/
  @ApiModelProperty(required = true, value = "the recipe name")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Recipe photo(List<String> photo) {
    this.photo = photo;
    return this;
  }

  public Recipe addPhotoItem(String photoItem) {
    this.photo.add(photoItem);
    return this;
  }

  /**
   * the filenames of the photo of the recipe
   * @return photo
  **/
  @ApiModelProperty(required = true, value = "the filenames of the photo of the recipe")
  @NotNull


  public List<String> getPhoto() {
    return photo;
  }

  public void setPhoto(List<String> photo) {
    this.photo = photo;
  }

  public Recipe standardPrice(double standardPrice) {
    this.standardPrice = standardPrice;
    return this;
  }

  /**
   * the basic price of the recipe
   * @return standardPrice
  **/
  @ApiModelProperty(required = true, value = "the basic price of the recipe")
  @NotNull


  public double getStandardPrice() {
    return standardPrice;
  }

  public void setStandardPrice(double standardPrice) {
    this.standardPrice = standardPrice;
  }

  public Recipe isStandard(Boolean isStandard) {
    this.isStandard = isStandard;
    return this;
  }

  /**
   * is this recipe part of the good food officials recipes
   * @return isStandard
  **/
  @ApiModelProperty(value = "is this recipe part of the good food officials recipes")


  public Boolean isIsStandard() {
    return isStandard;
  }

  public void setIsStandard(Boolean isStandard) {
    this.isStandard = isStandard;
  }

  public Recipe ingredients(List<StoreProduct> ingredients) {
    this.ingredients = ingredients;
    return this;
  }

  public Recipe addIngredientsItem(StoreProduct ingredientsItem) {
    this.ingredients.add(ingredientsItem);
    return this;
  }

  /**
   * the ingredients of the recipe
   * @return ingredients
  **/
  @ApiModelProperty(required = true, value = "the ingredients of the recipe")
  @NotNull


  public List<StoreProduct> getIngredients() {
    return ingredients;
  }

  public void setIngredients(List<StoreProduct> ingredients) {
    this.ingredients = ingredients;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Recipe recipe = (Recipe) o;
    return Objects.equals(this.id, recipe.id) &&
        Objects.equals(this.owner, recipe.owner) &&
        Objects.equals(this.name, recipe.name) &&
        Objects.equals(this.photo, recipe.photo) &&
        Objects.equals(this.standardPrice, recipe.standardPrice) &&
        Objects.equals(this.isStandard, recipe.isStandard) &&
        Objects.equals(this.ingredients, recipe.ingredients);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, owner, name, photo, standardPrice, isStandard, ingredients);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Recipe {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    photo: ").append(toIndentedString(photo)).append("\n");
    sb.append("    standardPrice: ").append(toIndentedString(standardPrice)).append("\n");
    sb.append("    isStandard: ").append(toIndentedString(isStandard)).append("\n");
    sb.append("    ingredients: ").append(toIndentedString(ingredients)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  public void setIngredientsByDocuments(List<Document> ingredientsDoc) {
	  System.out.println("Ingredient::setIngredientsByDocuments recipesDoc = " + ingredientsDoc.toString());
	  for (Iterator<Document> iterator = ingredientsDoc.iterator(); iterator.hasNext();) {
		Document document = (Document) iterator.next();
		StoreProduct myRecipeCellProduct = new StoreProduct(document);
		  this.ingredients.add(myRecipeCellProduct);
		
	}
	  
	  }
  public Document toDocument()
  {
	  Document documentToReturn = new Document();
	  documentToReturn.put("owner", this.owner);
	  documentToReturn.put("name", this.name);
	  documentToReturn.put("photo", this.photo);
	  documentToReturn.put("standardPrice", this.standardPrice);
	  documentToReturn.put("isStandard", this.isStandard);
	  List<Document> ingredientsDocuments = new ArrayList<Document>();
	  for(StoreProduct ingredient: this.ingredients)
	  {
		  ingredientsDocuments.add(ingredient.toDocument());
	  }
	  documentToReturn.put("ingredients", ingredientsDocuments);
	  return documentToReturn;
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.auth0.jwt.JWTCreator.Builder;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.CellProduct;
import io.swagger.buisness.types.ObjectIdSerializer2;
import io.swagger.buisness.types.Person;
import io.swagger.model.Address;
import net.bytebuddy.asm.Advice.This;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.threeten.bp.OffsetDateTime;
import org.bson.BsonArray;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * restaurant model
 */
@ApiModel(description = "restaurant model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Restaurant implements Serializable  {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@JsonProperty("id")
@Id
@JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("owner")
  private Person owner = null;

  @JsonProperty("address")
  private Address address = null;

  @JsonProperty("phone")
  private String phone = null;

  @JsonProperty("openDateTime")
  @Valid
  @JsonFormat(pattern="HH:mm")
  private List<Date> openDateTime = new ArrayList<Date>();

  @JsonProperty("closeDateTime")
  @Valid
  @JsonFormat(pattern="HH:mm")
  private List<Date> closeDateTime = new ArrayList<Date>();

  @JsonProperty("recipes")
  @Valid
  private List<CellProduct> recipes = new ArrayList<CellProduct>();

  @JsonProperty("suppliers")
  @Valid
  @JsonSerialize(contentUsing = ObjectIdSerializer2.class)
  private List<ObjectId> suppliers = new ArrayList<ObjectId>();

  public Restaurant id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the restaurant id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the restaurant id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Restaurant name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the restaurant name
   * @return name
  **/
  @ApiModelProperty(value = "the restaurant name")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Restaurant owner(Person owner) {
    this.owner = owner;
    return this;
  }

  /**
   * the restaurant owner
   * @return owner
  **/
  @ApiModelProperty(value = "the restaurant owner")


  public Person getOwner() {
    return owner;
  }

  public void setOwner(Person owner) {
    this.owner = owner;
  }

  public Restaurant address(Address address) {
    this.address = address;
    return this;
  }

  /**
   * the restaurant address
   * @return address
  **/
  @ApiModelProperty(required = true, value = "the restaurant address")
  @NotNull

  @Valid

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Restaurant phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * the restaurant phone number
   * @return phone
  **/
  @ApiModelProperty(required = true, value = "the restaurant phone number")
  @NotNull


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Restaurant openDateTime(List<Date> openDateTime) {
    this.openDateTime = openDateTime;
    return this;
  }

  public Restaurant addOpenDateTimeItem(Date openDateTimeItem) {
    this.openDateTime.add(openDateTimeItem);
    return this;
  }

  /**
   * the opening hours of the restaurant
   * @return openDateTime
  **/
  @ApiModelProperty(required = true, value = "the opening hours of the restaurant")
  @NotNull

  @Valid

  public List<Date> getOpenDateTime() {
    return openDateTime;
  }

  public void setOpenDateTime(List<Date> openDateTime) {
    this.openDateTime = openDateTime;
  }

  public Restaurant closeDateTime(List<Date> closeDateTime) {
    this.closeDateTime = closeDateTime;
    return this;
  }

  public Restaurant addCloseDateTimeItem(Date closeDateTimeItem) {
    this.closeDateTime.add(closeDateTimeItem);
    return this;
  }

  /**
   * the closing hours of the restaurant
   * @return closeDateTime
  **/
  @ApiModelProperty(required = true, value = "the closing hours of the restaurant")
  @NotNull

  @Valid

  public List<Date> getCloseDateTime() {
    return closeDateTime;
  }

  public void setCloseDateTime(List<Date> closeDateTime) {
    this.closeDateTime = closeDateTime;
  }

  public Restaurant recipes(List<CellProduct> recipes) {
    this.recipes = recipes;
    return this;
  }

  public Restaurant addRecipesItem(CellProduct recipesItem) {
    this.recipes.add(recipesItem);
    return this;
  }

  /**
   * the recipes provided by the restaurant
   * @return recipes
  **/
  @ApiModelProperty(required = true, value = "the recipes provided by the restaurant")
  @NotNull


  public List<CellProduct> getRecipes() {
    return recipes;
  }

  public void setRecipes(List<CellProduct> recipes) {
    this.recipes = recipes;
  }
  public void setRecipesByDocuments(List<Document> recipesDoc) {
	  System.out.println("Restaurant::setRecipesByDocuments recipesDoc = " + recipesDoc.toString());
	  /*Document RecipiesDoc = new Document((Map<String, Object>) recipes.get("owner"));
	  List<Object> valuesList =   recipes.getObjectId();
	  List<Object> valuesList =   recipes.getDouble("");
	  for(Object value : valuesList)
	  {
		  recipes.add(value.);
	  }
	  
	    this.recipes = */
	  for (Iterator<Document> iterator = recipesDoc.iterator(); iterator.hasNext();) {
		Document document = (Document) iterator.next();
		CellProduct myRecipeCellProduct = new CellProduct(document);
		  this.recipes.add(myRecipeCellProduct);
		
	}
		  /*for(Document recipeDocument : recipesDoc)
		  {
			  CellProduct myRecipeCellProduct = new CellProduct(recipeDocument);
			  this.recipes.add(myRecipeCellProduct);
		  }*/
	  
	  }

  public Restaurant suppliers(List<ObjectId> suppliers) {
    this.suppliers = suppliers;
    return this;
  }

  public Restaurant addSuppliersItem(ObjectId suppliersItem) {
    this.suppliers.add(suppliersItem);
    return this;
  }

  /**
   * the suppliers the restaurant is working with
   * @return suppliers
  **/
  @ApiModelProperty(required = true, value = "the suppliers the restaurant is working with")
  @NotNull


  public List<ObjectId> getSuppliers() {
    return suppliers;
  }

  public void setSuppliers(List<ObjectId> suppliers) {
    this.suppliers = suppliers;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Restaurant restaurant = (Restaurant) o;
    return Objects.equals(this.id, restaurant.id) &&
        Objects.equals(this.name, restaurant.name) &&
        Objects.equals(this.owner, restaurant.owner) &&
        Objects.equals(this.address, restaurant.address) &&
        Objects.equals(this.phone, restaurant.phone) &&
        Objects.equals(this.openDateTime, restaurant.openDateTime) &&
        Objects.equals(this.closeDateTime, restaurant.closeDateTime) &&
        Objects.equals(this.recipes, restaurant.recipes) &&
        Objects.equals(this.suppliers, restaurant.suppliers);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, owner, address, phone, openDateTime, closeDateTime, recipes, suppliers);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("{\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    openDateTime: ").append(toIndentedString(openDateTime)).append("\n");
    sb.append("    closeDateTime: ").append(toIndentedString(closeDateTime)).append("\n");
    sb.append("    recipes: ").append(toIndentedString(recipes)).append("\n");
    sb.append("    suppliers: ").append(toIndentedString(suppliers)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  public Document toDocument()
  {
	  DateFormat simpleDateFormat= new SimpleDateFormat("dd/MM/yyyy");
	  Document doc = new Document();
	  doc.put("name", this.name);
	  doc.put("owner", this.owner.toDocument());
	  doc.put("address", this.address.toDocument());
	  doc.put("phone", this.phone);
	  List<Document> openDateTimeDocuments = new ArrayList<Document>();
	  
	  doc.put("openDateTime", /*dateToDocument(*/this.openDateTime/*)*/);
	  List<Document> closeDateTimeDocuments = new ArrayList<Document>();
	  doc.put("closeDateTime", /*dateToDocument(*/this.closeDateTime/*)*/);
	  List<Document> recipesDocuments = new ArrayList<Document>();
	  for(CellProduct recipe: this.recipes)
	  {
		  recipesDocuments.add(recipe.toDocument());
	  }
	  
	  doc.put("recipes", recipesDocuments);
	  doc.put("suppliers", this.suppliers);
	   return doc;
	  
  }
  private Document dateToDocument(List<Date> object)
  {
	  DateFormat simpleDateFormat= new SimpleDateFormat("HH:mm");
	  Document documentListToReturn = new Document();
	  for(Date oneopenDateTime: this.openDateTime)
	  {
		  documentListToReturn.append("",simpleDateFormat.format(oneopenDateTime));
	  }
	
	  
	  return documentListToReturn;
  }
  private Document ListOfCellProductToDocumentList(List<CellProduct> cellProductList)
  {
	  Document documentListToReturn = new Document();
	  
	  for(CellProduct object : cellProductList)
	  {
		  documentListToReturn.append("",object.toDocument());
	  }
	  
	  return documentListToReturn;
  }
  private Document listOfObjectIdToDocumentList(List<ObjectId> objectIdList)
  {
	  Document documentListToReturn = new Document();
	  for(ObjectId object : objectIdList)
	  {
		  documentListToReturn.append("",object);
	  }
	  
	  return documentListToReturn;
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;
import io.swagger.model.Address;
import io.swagger.model.Delivery;
import io.swagger.model.OrderDetail;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * customer order model
 */
@ApiModel(description = "customer order model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Order   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("customer")
  private String customer = null;

  @JsonProperty("address")
  private Address address = null;

  @JsonProperty("restaurant")
  private String restaurant = null;

  @JsonProperty("discount")
  private String discount = null;

  @JsonProperty("orderDate")
  private OffsetDateTime orderDate = null;

  @JsonProperty("total")
  private Integer total = null;

  @JsonProperty("isPayed")
  private Boolean isPayed = null;

  @JsonProperty("paymentRef")
  private String paymentRef = null;

  /**
   * the status of the order in its process
   */
  public enum StatusEnum {
    PLACED("placed"),
    
    APPROVED("approved"),
    
    PROVIDED("provided"),
    
    DELIVERED("delivered");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("details")
  @Valid
  private List<OrderDetail> details = new ArrayList<OrderDetail>();

  @JsonProperty("delivery")
  private Delivery delivery = null;

  public Order id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * order unique id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "order unique id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Order customer(String customer) {
    this.customer = customer;
    return this;
  }

  /**
   * the id of the customer who passes the order
   * @return customer
  **/
  @ApiModelProperty(required = true, value = "the id of the customer who passes the order")
  @NotNull


  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  public Order address(Address address) {
    this.address = address;
    return this;
  }

  /**
   * the adress the order will be delivered to
   * @return address
  **/
  @ApiModelProperty(required = true, value = "the adress the order will be delivered to")
  @NotNull

  @Valid

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Order restaurant(String restaurant) {
    this.restaurant = restaurant;
    return this;
  }

  /**
   * the id of the restaurant which receive the order
   * @return restaurant
  **/
  @ApiModelProperty(required = true, value = "the id of the restaurant which receive the order")
  @NotNull


  public String getRestaurant() {
    return restaurant;
  }

  public void setRestaurant(String restaurant) {
    this.restaurant = restaurant;
  }

  public Order discount(String discount) {
    this.discount = discount;
    return this;
  }

  /**
   * the id of a discount that is applied to this order, if need be
   * @return discount
  **/
  @ApiModelProperty(value = "the id of a discount that is applied to this order, if need be")


  public String getDiscount() {
    return discount;
  }

  public void setDiscount(String discount) {
    this.discount = discount;
  }

  public Order orderDate(OffsetDateTime orderDate) {
    this.orderDate = orderDate;
    return this;
  }

  /**
   * the date of the order
   * @return orderDate
  **/
  @ApiModelProperty(required = true, value = "the date of the order")
  @NotNull

  @Valid

  public OffsetDateTime getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(OffsetDateTime orderDate) {
    this.orderDate = orderDate;
  }

  public Order total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * the total amount for the order, sum of the order details amounts
   * @return total
  **/
  @ApiModelProperty(required = true, value = "the total amount for the order, sum of the order details amounts")
  @NotNull


  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public Order isPayed(Boolean isPayed) {
    this.isPayed = isPayed;
    return this;
  }

  /**
   * set to true when the customer payment is confirm
   * @return isPayed
  **/
  @ApiModelProperty(required = true, value = "set to true when the customer payment is confirm")
  @NotNull


  public Boolean isIsPayed() {
    return isPayed;
  }

  public void setIsPayed(Boolean isPayed) {
    this.isPayed = isPayed;
  }

  public Order paymentRef(String paymentRef) {
    this.paymentRef = paymentRef;
    return this;
  }

  /**
   * the reference of the payment from BNB
   * @return paymentRef
  **/
  @ApiModelProperty(value = "the reference of the payment from BNB")


  public String getPaymentRef() {
    return paymentRef;
  }

  public void setPaymentRef(String paymentRef) {
    this.paymentRef = paymentRef;
  }

  public Order status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * the status of the order in its process
   * @return status
  **/
  @ApiModelProperty(required = true, value = "the status of the order in its process")
  @NotNull


  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Order details(List<OrderDetail> details) {
    this.details = details;
    return this;
  }

  public Order addDetailsItem(OrderDetail detailsItem) {
    this.details.add(detailsItem);
    return this;
  }

  /**
   * the details of the order for each recipe ordered
   * @return details
  **/
  @ApiModelProperty(required = true, value = "the details of the order for each recipe ordered")
  @NotNull

  @Valid

  public List<OrderDetail> getDetails() {
    return details;
  }

  public void setDetails(List<OrderDetail> details) {
    this.details = details;
  }

  public Order delivery(Delivery delivery) {
    this.delivery = delivery;
    return this;
  }

  /**
   * the delivery informations of the order if needed
   * @return delivery
  **/
  @ApiModelProperty(required = true, value = "the delivery informations of the order if needed")
  @NotNull

  @Valid

  public Delivery getDelivery() {
    return delivery;
  }

  public void setDelivery(Delivery delivery) {
    this.delivery = delivery;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(this.id, order.id) &&
        Objects.equals(this.customer, order.customer) &&
        Objects.equals(this.address, order.address) &&
        Objects.equals(this.restaurant, order.restaurant) &&
        Objects.equals(this.discount, order.discount) &&
        Objects.equals(this.orderDate, order.orderDate) &&
        Objects.equals(this.total, order.total) &&
        Objects.equals(this.isPayed, order.isPayed) &&
        Objects.equals(this.paymentRef, order.paymentRef) &&
        Objects.equals(this.status, order.status) &&
        Objects.equals(this.details, order.details) &&
        Objects.equals(this.delivery, order.delivery);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, customer, address, restaurant, discount, orderDate, total, isPayed, paymentRef, status, details, delivery);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Order {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    customer: ").append(toIndentedString(customer)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    restaurant: ").append(toIndentedString(restaurant)).append("\n");
    sb.append("    discount: ").append(toIndentedString(discount)).append("\n");
    sb.append("    orderDate: ").append(toIndentedString(orderDate)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    isPayed: ").append(toIndentedString(isPayed)).append("\n");
    sb.append("    paymentRef: ").append(toIndentedString(paymentRef)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("    delivery: ").append(toIndentedString(delivery)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


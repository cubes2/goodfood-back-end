package io.swagger.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * address model
 */
@ApiModel(description = "address model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Address   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("location")
  private String location = null;

  @JsonProperty("number")
  private String number = null;

  @JsonProperty("street")
  private String street = null;

  @JsonProperty("postal")
  private String postal = null;

  @JsonProperty("city")
  private String city = null;

  public Address id(ObjectId id) {
    this.id = id;
    return this;
  }
  public Address(Document AddressDoc)
  {
	  System.out.println("Address::Address AddressDoc = " + AddressDoc.toString());
	  setCity(AddressDoc.getString("city"));
	  try {
		  setId(AddressDoc.getObjectId("id"));
	} catch (Exception e) {
		setId(new ObjectId(AddressDoc.getString("id")));
		// TODO: handle exception
	}
	  
	  setLocation(AddressDoc.getString("location"));
	  setNumber(AddressDoc.getString("number"));
	  setPostal(AddressDoc.getString("postal"));
	  setStreet(AddressDoc.getString("street"));
	  
  }

  /**
   * the address id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the address id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Address location(String location) {
    this.location = location;
    return this;
  }

  /**
   * the gps location of the address
   * @return location
  **/
  @ApiModelProperty(required = true, value = "the gps location of the address")
  @NotNull


  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public Address number(String number) {
    this.number = number;
    return this;
  }

  /**
   * the street number of the address
   * @return number
  **/
  @ApiModelProperty(value = "the street number of the address")


  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Address street(String street) {
    this.street = street;
    return this;
  }

  /**
   * the street part of the address
   * @return street
  **/
  @ApiModelProperty(value = "the street part of the address")


  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public Address postal(String postal) {
    this.postal = postal;
    return this;
  }

  /**
   * the postal code
   * @return postal
  **/
  @ApiModelProperty(value = "the postal code")


  public String getPostal() {
    return postal;
  }

  public void setPostal(String postal) {
    this.postal = postal;
  }

  public Address city(String city) {
    this.city = city;
    return this;
  }

  /**
   * the city part of the address
   * @return city
  **/
  @ApiModelProperty(value = "the city part of the address")


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return Objects.equals(this.id, address.id) &&
        Objects.equals(this.location, address.location) &&
        Objects.equals(this.number, address.number) &&
        Objects.equals(this.street, address.street) &&
        Objects.equals(this.postal, address.postal) &&
        Objects.equals(this.city, address.city);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, location, number, street, postal, city);
  }
  public Document toDocument()
  {
	  Document DocumentToReturn = new Document();
	  DocumentToReturn.put("id", this.id);
	  DocumentToReturn.put("location", this.location);
	  DocumentToReturn.put("number", this.number);
	  DocumentToReturn.put("street", this.street);
	  DocumentToReturn.put("postal", this.postal);
	  DocumentToReturn.put("city", this.city);
		return DocumentToReturn;
	  
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Address {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    street: ").append(toIndentedString(street)).append("\n");
    sb.append("    postal: ").append(toIndentedString(postal)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


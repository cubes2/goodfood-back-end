package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;

import org.threeten.bp.OffsetDateTime;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * discount model
 */
@ApiModel(description = "discount model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Discount   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("recipe")
  private String recipe = null;

  @JsonProperty("value")
  private Integer value = null;

  @JsonProperty("start")
  private OffsetDateTime start = null;

  @JsonProperty("end")
  private OffsetDateTime end = null;

  public Discount id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the discount id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the discount id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Discount recipe(String recipe) {
    this.recipe = recipe;
    return this;
  }

  /**
   * the id of the recipe concern by the discount if needed
   * @return recipe
  **/
  @ApiModelProperty(value = "the id of the recipe concern by the discount if needed")


  public String getRecipe() {
    return recipe;
  }

  public void setRecipe(String recipe) {
    this.recipe = recipe;
  }

  public Discount value(Integer value) {
    this.value = value;
    return this;
  }

  /**
   * the discount value
   * @return value
  **/
  @ApiModelProperty(required = true, value = "the discount value")
  @NotNull


  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  public Discount start(OffsetDateTime start) {
    this.start = start;
    return this;
  }

  /**
   * the date the discount start
   * @return start
  **/
  @ApiModelProperty(required = true, value = "the date the discount start")
  @NotNull

  @Valid

  public OffsetDateTime getStart() {
    return start;
  }

  public void setStart(OffsetDateTime start) {
    this.start = start;
  }

  public Discount end(OffsetDateTime end) {
    this.end = end;
    return this;
  }

  /**
   * the date the discount end
   * @return end
  **/
  @ApiModelProperty(required = true, value = "the date the discount end")
  @NotNull

  @Valid

  public OffsetDateTime getEnd() {
    return end;
  }

  public void setEnd(OffsetDateTime end) {
    this.end = end;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Discount discount = (Discount) o;
    return Objects.equals(this.id, discount.id) &&
        Objects.equals(this.recipe, discount.recipe) &&
        Objects.equals(this.value, discount.value) &&
        Objects.equals(this.start, discount.start) &&
        Objects.equals(this.end, discount.end);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, recipe, value, start, end);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Discount {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    recipe: ").append(toIndentedString(recipe)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    end: ").append(toIndentedString(end)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


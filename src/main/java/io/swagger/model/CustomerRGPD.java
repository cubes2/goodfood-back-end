package io.swagger.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.bson.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * customer RGPD model
 */
@ApiModel(description = "customer RGPD model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class CustomerRGPD   {
  @JsonProperty("hasConsentForDB")
  private Boolean hasConsentForDB = null;

  @DateTimeFormat(pattern = "dd/MM/yyyy")
  @JsonFormat(pattern="dd/MM/yyyy")
  @JsonProperty("dbConsentDate")
  private Date dbConsentDate = null;

  @JsonProperty("hasConsentForCookies")
  private Boolean hasConsentForCookies = null;

  @DateTimeFormat(pattern = "dd/MM/yyyy")
  @JsonProperty("cookieConsentDate")
  @JsonFormat(pattern="dd/MM/yyyy")
  private Date cookieConsentDate = null;
  
  public CustomerRGPD(Document customerRgpdDoc)
  {
	  System.out.println("CustomerRGPD::CustomerRGPD customerRgpdDoc = " + customerRgpdDoc.toString());
	  DateFormat simpleDateFormat= new SimpleDateFormat("dd/MM/yyyy");
	  try 
	  {
		setCookieConsentDate(simpleDateFormat.parse((String) customerRgpdDoc.get("cookieConsentDate")));
		setDbConsentDate(simpleDateFormat.parse((String) customerRgpdDoc.get("dbConsentDate")));
		setHasConsentForCookies(customerRgpdDoc.getBoolean("hasConsentForCookies"));
		setHasConsentForDB(customerRgpdDoc.getBoolean("hasConsentForDB"));
	  } 
	  catch (ParseException e) 
	  {
			// TODO Auto-generated catch block
		  System.out.println("CustomerRGPD::CustomerRGPD ERROR = ");
	  }
	  
  }

  public CustomerRGPD hasConsentForDB(Boolean hasConsentForDB) {
    this.hasConsentForDB = hasConsentForDB;
    return this;
  }

  /**
   * has customer consent for his personal data to be stored in a database
   * @return hasConsentForDB
  **/
  @ApiModelProperty(required = true, value = "has customer consent for his personal data to be stored in a database")
  @NotNull


  public Boolean isHasConsentForDB() {
    return hasConsentForDB;
  }

  public void setHasConsentForDB(Boolean hasConsentForDB) {
    this.hasConsentForDB = hasConsentForDB;
  }

  public CustomerRGPD dbConsentDate(Date dbConsentDate) {
    this.dbConsentDate = dbConsentDate;
    return this;
  }

  /**
   * database storage consent date
   * @return dbConsentDate
  **/
  @ApiModelProperty(required = true, value = "database storage consent date")
  @NotNull

  @Valid

  public Date getDbConsentDate() {
    return dbConsentDate;
  }

  public void setDbConsentDate(Date dbConsentDate) {
    this.dbConsentDate = dbConsentDate;
  }

  public CustomerRGPD hasConsentForCookies(Boolean hasConsentForCookies) {
    this.hasConsentForCookies = hasConsentForCookies;
    return this;
  }

  /**
   * has customer consent for the use of cookies by the application
   * @return hasConsentForCookies
  **/
  @ApiModelProperty(required = true, value = "has customer consent for the use of cookies by the application")
  @NotNull


  public Boolean isHasConsentForCookies() {
    return hasConsentForCookies;
  }

  public void setHasConsentForCookies(Boolean hasConsentForCookies) {
    this.hasConsentForCookies = hasConsentForCookies;
  }

  public CustomerRGPD cookieConsentDate(Date cookieConsentDate) {
    this.cookieConsentDate = cookieConsentDate;
    return this;
  }

  /**
   * cookie usage consent date
   * @return cookieConsentDate
  **/
  @ApiModelProperty(required = true, value = "cookie usage consent date")
  @NotNull

  @Valid

  public Date getCookieConsentDate() {
    return cookieConsentDate;
  }

  public void setCookieConsentDate(Date cookieConsentDate) {
    this.cookieConsentDate = cookieConsentDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomerRGPD customerRGPD = (CustomerRGPD) o;
    return Objects.equals(this.hasConsentForDB, customerRGPD.hasConsentForDB) &&
        Objects.equals(this.dbConsentDate, customerRGPD.dbConsentDate) &&
        Objects.equals(this.hasConsentForCookies, customerRGPD.hasConsentForCookies) &&
        Objects.equals(this.cookieConsentDate, customerRGPD.cookieConsentDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(hasConsentForDB, dbConsentDate, hasConsentForCookies, cookieConsentDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomerRGPD {\n");
    
    sb.append("    hasConsentForDB: ").append(toIndentedString(hasConsentForDB)).append("\n");
    sb.append("    dbConsentDate: ").append(toIndentedString(dbConsentDate)).append("\n");
    sb.append("    hasConsentForCookies: ").append(toIndentedString(hasConsentForCookies)).append("\n");
    sb.append("    cookieConsentDate: ").append(toIndentedString(cookieConsentDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


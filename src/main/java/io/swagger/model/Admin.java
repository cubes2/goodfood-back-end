package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * admin model
 */
@ApiModel(description = "admin model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Admin   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("login")
  private String login = null;

  @JsonProperty("password")
  private String password = null;

  @JsonProperty("permission")
  @Valid
  private List<String> permission = new ArrayList<String>();

  public Admin id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the admin id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the admin id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Admin login(String login) {
    this.login = login;
    return this;
  }

  /**
   * the admin login
   * @return login
  **/
  @ApiModelProperty(required = true, value = "the admin login")
  @NotNull


  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public Admin password(String password) {
    this.password = password;
    return this;
  }

  /**
   * the admin password
   * @return password
  **/
  @ApiModelProperty(required = true, value = "the admin password")
  @NotNull


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Admin permission(List<String> permission) {
    this.permission = permission;
    return this;
  }

  public Admin addPermissionItem(String permissionItem) {
    this.permission.add(permissionItem);
    return this;
  }

  /**
   * the admin permissions
   * @return permission
  **/
  @ApiModelProperty(required = true, value = "the admin permissions")
  @NotNull


  public List<String> getPermission() {
    return permission;
  }

  public void setPermission(List<String> permission) {
    this.permission = permission;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Admin admin = (Admin) o;
    return Objects.equals(this.id, admin.id) &&
        Objects.equals(this.login, admin.login) &&
        Objects.equals(this.password, admin.password) &&
        Objects.equals(this.permission, admin.permission);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, login, password, permission);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Admin {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    permission: ").append(toIndentedString(permission)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


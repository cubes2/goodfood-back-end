package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.CellProduct;
import io.swagger.buisness.types.ObjectIdSerializer2;
import net.bytebuddy.asm.Advice.This;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ingredient model
 */
@ApiModel(description = "ingredient model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Ingredient   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("allergen")
  @Valid
  private List<String> allergen = null;

  public Ingredient id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the ingredient id
   * @return id
  **/
  @ApiModelProperty(value = "the ingredient id")


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Ingredient name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the ingredient name
   * @return name
  **/
  @ApiModelProperty(value = "the ingredient name")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Ingredient allergen(List<String> allergen) {
    this.allergen = allergen;
    return this;
  }

  public Ingredient addAllergenItem(String allergenItem) {
    if (this.allergen == null) {
      this.allergen = new ArrayList<String>();
    }
    this.allergen.add(allergenItem);
    return this;
  }

  /**
   * the ingredient known allergen
   * @return allergen
  **/
  @ApiModelProperty(value = "the ingredient known allergen")


  public List<String> getAllergen() {
    return allergen;
  }

  public void setAllergen(List<String> allergen) {
    this.allergen = allergen;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ingredient ingredient = (Ingredient) o;
    return Objects.equals(this.id, ingredient.id) &&
        Objects.equals(this.name, ingredient.name) &&
        Objects.equals(this.allergen, ingredient.allergen);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, allergen);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Ingredient {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    allergen: ").append(toIndentedString(allergen)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public Document toDocument()
  {
	  Document documentToReturn = new Document();
	  documentToReturn.put("name", this.name);
	  documentToReturn.put("allergen", this.allergen);
	  return documentToReturn;
  }
}


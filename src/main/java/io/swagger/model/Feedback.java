package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;

import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * feedback model
 */
@ApiModel(description = "feedback model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Feedback   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("customer")
  private String customer = null;

  @JsonProperty("comment")
  private String comment = null;

  @JsonProperty("eval")
  private Integer eval = null;

  /**
   * the type of target of the feedBack
   */
  public enum TypeEnum {
    ORDER("order"),
    
    DELIVERY("delivery"),
    
    RECIPE("recipe"),
    
    RESTAURANT("restaurant");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("type")
  private TypeEnum type = null;

  @JsonProperty("target")
  private String target = null;

  public Feedback id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the feedback id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the feedback id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Feedback customer(String customer) {
    this.customer = customer;
    return this;
  }

  /**
   * the id of the customer who made the feedback
   * @return customer
  **/
  @ApiModelProperty(required = true, value = "the id of the customer who made the feedback")
  @NotNull


  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  public Feedback comment(String comment) {
    this.comment = comment;
    return this;
  }

  /**
   * the comment of the feedback
   * @return comment
  **/
  @ApiModelProperty(required = true, value = "the comment of the feedback")
  @NotNull


  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Feedback eval(Integer eval) {
    this.eval = eval;
    return this;
  }

  /**
   * the customer evaluation of the service quality
   * @return eval
  **/
  @ApiModelProperty(value = "the customer evaluation of the service quality")


  public Integer getEval() {
    return eval;
  }

  public void setEval(Integer eval) {
    this.eval = eval;
  }

  public Feedback type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * the type of target of the feedBack
   * @return type
  **/
  @ApiModelProperty(required = true, value = "the type of target of the feedBack")
  @NotNull


  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Feedback target(String target) {
    this.target = target;
    return this;
  }

  /**
   * the id of the target of the feedback
   * @return target
  **/
  @ApiModelProperty(required = true, value = "the id of the target of the feedback")
  @NotNull


  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Feedback feedback = (Feedback) o;
    return Objects.equals(this.id, feedback.id) &&
        Objects.equals(this.customer, feedback.customer) &&
        Objects.equals(this.comment, feedback.comment) &&
        Objects.equals(this.eval, feedback.eval) &&
        Objects.equals(this.type, feedback.type) &&
        Objects.equals(this.target, feedback.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, customer, comment, eval, type, target);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Feedback {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    customer: ").append(toIndentedString(customer)).append("\n");
    sb.append("    comment: ").append(toIndentedString(comment)).append("\n");
    sb.append("    eval: ").append(toIndentedString(eval)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    target: ").append(toIndentedString(target)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


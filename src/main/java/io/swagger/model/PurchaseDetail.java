package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Purchase detail model
 */
@ApiModel(description = "Purchase detail model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class PurchaseDetail   {
  @JsonProperty("ingredientSupplierId")
  private String ingredientSupplierId = null;

  @JsonProperty("quantity")
  private Integer quantity = null;

  @JsonProperty("price")
  private Integer price = null;

  @JsonProperty("total")
  private Integer total = null;

  public PurchaseDetail ingredientSupplierId(String ingredientSupplierId) {
    this.ingredientSupplierId = ingredientSupplierId;
    return this;
  }

  /**
   * the id of the ingredient supplier ordered
   * @return ingredientSupplierId
  **/
  @ApiModelProperty(required = true, value = "the id of the ingredient supplier ordered")
  @NotNull


  public String getIngredientSupplierId() {
    return ingredientSupplierId;
  }

  public void setIngredientSupplierId(String ingredientSupplierId) {
    this.ingredientSupplierId = ingredientSupplierId;
  }

  public PurchaseDetail quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * quantity ordered for this purchase
   * @return quantity
  **/
  @ApiModelProperty(required = true, value = "quantity ordered for this purchase")
  @NotNull


  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public PurchaseDetail price(Integer price) {
    this.price = price;
    return this;
  }

  /**
   * the price of the purchase at the date of the order, potential discount included
   * @return price
  **/
  @ApiModelProperty(required = true, value = "the price of the purchase at the date of the order, potential discount included")
  @NotNull


  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public PurchaseDetail total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * the total amount ordered for this purchase
   * @return total
  **/
  @ApiModelProperty(required = true, value = "the total amount ordered for this purchase")
  @NotNull


  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PurchaseDetail purchaseDetail = (PurchaseDetail) o;
    return Objects.equals(this.ingredientSupplierId, purchaseDetail.ingredientSupplierId) &&
        Objects.equals(this.quantity, purchaseDetail.quantity) &&
        Objects.equals(this.price, purchaseDetail.price) &&
        Objects.equals(this.total, purchaseDetail.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ingredientSupplierId, quantity, price, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PurchaseDetail {\n");
    
    sb.append("    ingredientSupplierId: ").append(toIndentedString(ingredientSupplierId)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


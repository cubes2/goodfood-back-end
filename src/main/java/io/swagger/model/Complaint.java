package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * complaint model
 */
@ApiModel(description = "complaint model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Complaint   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("customer")
  private String customer = null;

  @JsonProperty("order")
  private String order = null;

  /**
   * the type of the complaint
   */
  public enum TypeEnum {
    NOT_DELIVERED("not_delivered"),
    
    LATE("late"),
    
    MISSING_PRODUCT("missing_product"),
    
    WRONG_PRODUCT("wrong_product"),
    
    BAD_FOOD_QUALITY("bad_food_quality");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("type")
  private TypeEnum type = null;

  @JsonProperty("photo")
  @Valid
  private List<String> photo = null;

  @JsonProperty("description")
  private String description = null;

  /**
   * community management Response to the complaint
   */
  public enum StatusEnum {
    PROCESSING("processing"),
    
    APPROVED("approved"),
    
    DENIED("denied"),
    
    PROVIDED("provided");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  /**
   * action taken for approved complaints
   */
  public enum ActionEnum {
    REFUND("refund"),
    
    VOUCHER("voucher");

    private String value;

    ActionEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ActionEnum fromValue(String text) {
      for (ActionEnum b : ActionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("action")
  private ActionEnum action = null;

  @JsonProperty("value")
  private Integer value = null;

  public Complaint id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the complaint id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the complaint id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Complaint customer(String customer) {
    this.customer = customer;
    return this;
  }

  /**
   * the id of the customer who made the complaint
   * @return customer
  **/
  @ApiModelProperty(required = true, value = "the id of the customer who made the complaint")
  @NotNull


  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  public Complaint order(String order) {
    this.order = order;
    return this;
  }

  /**
   * the id of the order concern by the complaint
   * @return order
  **/
  @ApiModelProperty(required = true, value = "the id of the order concern by the complaint")
  @NotNull


  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }

  public Complaint type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * the type of the complaint
   * @return type
  **/
  @ApiModelProperty(required = true, value = "the type of the complaint")
  @NotNull


  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Complaint photo(List<String> photo) {
    this.photo = photo;
    return this;
  }

  public Complaint addPhotoItem(String photoItem) {
    if (this.photo == null) {
      this.photo = new ArrayList<String>();
    }
    this.photo.add(photoItem);
    return this;
  }

  /**
   * the filename of the photo provided with complaint
   * @return photo
  **/
  @ApiModelProperty(value = "the filename of the photo provided with complaint")


  public List<String> getPhoto() {
    return photo;
  }

  public void setPhoto(List<String> photo) {
    this.photo = photo;
  }

  public Complaint description(String description) {
    this.description = description;
    return this;
  }

  /**
   * the explanation of the customer about the complaint
   * @return description
  **/
  @ApiModelProperty(required = true, value = "the explanation of the customer about the complaint")
  @NotNull


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Complaint status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * community management Response to the complaint
   * @return status
  **/
  @ApiModelProperty(required = true, value = "community management Response to the complaint")
  @NotNull


  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Complaint action(ActionEnum action) {
    this.action = action;
    return this;
  }

  /**
   * action taken for approved complaints
   * @return action
  **/
  @ApiModelProperty(value = "action taken for approved complaints")


  public ActionEnum getAction() {
    return action;
  }

  public void setAction(ActionEnum action) {
    this.action = action;
  }

  public Complaint value(Integer value) {
    this.value = value;
    return this;
  }

  /**
   * the monetary refund provided to the customer
   * @return value
  **/
  @ApiModelProperty(value = "the monetary refund provided to the customer")


  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Complaint complaint = (Complaint) o;
    return Objects.equals(this.id, complaint.id) &&
        Objects.equals(this.customer, complaint.customer) &&
        Objects.equals(this.order, complaint.order) &&
        Objects.equals(this.type, complaint.type) &&
        Objects.equals(this.photo, complaint.photo) &&
        Objects.equals(this.description, complaint.description) &&
        Objects.equals(this.status, complaint.status) &&
        Objects.equals(this.action, complaint.action) &&
        Objects.equals(this.value, complaint.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, customer, order, type, photo, description, status, action, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Complaint {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    customer: ").append(toIndentedString(customer)).append("\n");
    sb.append("    order: ").append(toIndentedString(order)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    photo: ").append(toIndentedString(photo)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


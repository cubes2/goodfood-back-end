package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;
import io.swagger.model.PurchaseDetail;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * purchase model
 */
@ApiModel(description = "purchase model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Purchase   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("restaurant")
  private String restaurant = null;

  @JsonProperty("supplier")
  private String supplier = null;

  @JsonProperty("orderDate")
  private OffsetDateTime orderDate = null;

  @JsonProperty("total")
  private Integer total = null;

  @JsonProperty("PurchaseDetails")
  @Valid
  private List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>();

  public Purchase id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the purchase id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the purchase id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Purchase restaurant(String restaurant) {
    this.restaurant = restaurant;
    return this;
  }

  /**
   * the id of the restaurant that passed this purchase
   * @return restaurant
  **/
  @ApiModelProperty(required = true, value = "the id of the restaurant that passed this purchase")
  @NotNull


  public String getRestaurant() {
    return restaurant;
  }

  public void setRestaurant(String restaurant) {
    this.restaurant = restaurant;
  }

  public Purchase supplier(String supplier) {
    this.supplier = supplier;
    return this;
  }

  /**
   * the id of the supplier this purchase is addressed to
   * @return supplier
  **/
  @ApiModelProperty(required = true, value = "the id of the supplier this purchase is addressed to")
  @NotNull


  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }

  public Purchase orderDate(OffsetDateTime orderDate) {
    this.orderDate = orderDate;
    return this;
  }

  /**
   * the date of the purchase
   * @return orderDate
  **/
  @ApiModelProperty(required = true, value = "the date of the purchase")
  @NotNull

  @Valid

  public OffsetDateTime getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(OffsetDateTime orderDate) {
    this.orderDate = orderDate;
  }

  public Purchase total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * the total amount for the purchase
   * @return total
  **/
  @ApiModelProperty(required = true, value = "the total amount for the purchase")
  @NotNull


  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public Purchase purchaseDetails(List<PurchaseDetail> purchaseDetails) {
    this.purchaseDetails = purchaseDetails;
    return this;
  }

  public Purchase addPurchaseDetailsItem(PurchaseDetail purchaseDetailsItem) {
    this.purchaseDetails.add(purchaseDetailsItem);
    return this;
  }

  /**
   * the details of the purchase for each ingredients
   * @return purchaseDetails
  **/
  @ApiModelProperty(required = true, value = "the details of the purchase for each ingredients")
  @NotNull

  @Valid

  public List<PurchaseDetail> getPurchaseDetails() {
    return purchaseDetails;
  }

  public void setPurchaseDetails(List<PurchaseDetail> purchaseDetails) {
    this.purchaseDetails = purchaseDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Purchase purchase = (Purchase) o;
    return Objects.equals(this.id, purchase.id) &&
        Objects.equals(this.restaurant, purchase.restaurant) &&
        Objects.equals(this.supplier, purchase.supplier) &&
        Objects.equals(this.orderDate, purchase.orderDate) &&
        Objects.equals(this.total, purchase.total) &&
        Objects.equals(this.purchaseDetails, purchase.purchaseDetails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, restaurant, supplier, orderDate, total, purchaseDetails);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Purchase {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    restaurant: ").append(toIndentedString(restaurant)).append("\n");
    sb.append("    supplier: ").append(toIndentedString(supplier)).append("\n");
    sb.append("    orderDate: ").append(toIndentedString(orderDate)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    purchaseDetails: ").append(toIndentedString(purchaseDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


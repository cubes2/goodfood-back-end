package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * order detail model
 */
@ApiModel(description = "order detail model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class OrderDetail   {
  @JsonProperty("recipe")
  private String recipe = null;

  @JsonProperty("quantity")
  private Integer quantity = null;

  @JsonProperty("price")
  private Integer price = null;

  @JsonProperty("total")
  private Integer total = null;

  public OrderDetail recipe(String recipe) {
    this.recipe = recipe;
    return this;
  }

  /**
   * the id of the recipe ordered
   * @return recipe
  **/
  @ApiModelProperty(required = true, value = "the id of the recipe ordered")
  @NotNull


  public String getRecipe() {
    return recipe;
  }

  public void setRecipe(String recipe) {
    this.recipe = recipe;
  }

  public OrderDetail quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * quantity ordered for this recipe
   * @return quantity
  **/
  @ApiModelProperty(required = true, value = "quantity ordered for this recipe")
  @NotNull


  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public OrderDetail price(Integer price) {
    this.price = price;
    return this;
  }

  /**
   * the price of the recipe at the date of the order, potential discount included
   * @return price
  **/
  @ApiModelProperty(required = true, value = "the price of the recipe at the date of the order, potential discount included")
  @NotNull


  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public OrderDetail total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * the total amount ordered for this recipe
   * @return total
  **/
  @ApiModelProperty(required = true, value = "the total amount ordered for this recipe")
  @NotNull


  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrderDetail orderDetail = (OrderDetail) o;
    return Objects.equals(this.recipe, orderDetail.recipe) &&
        Objects.equals(this.quantity, orderDetail.quantity) &&
        Objects.equals(this.price, orderDetail.price) &&
        Objects.equals(this.total, orderDetail.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(recipe, quantity, price, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrderDetail {\n");
    
    sb.append("    recipe: ").append(toIndentedString(recipe)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * supplier model
 */
@ApiModel(description = "supplier model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Supplier   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("isStandard")
  private Boolean isStandard = null;

  @JsonProperty("phone")
  private String phone = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("ingredients")
  @Valid
  private List<Ingredient> ingredients = new ArrayList<Ingredient>();

  public Supplier id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the supplier id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the supplier id")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Supplier name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the supplier name
   * @return name
  **/
  @ApiModelProperty(value = "the supplier name")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Supplier isStandard(Boolean isStandard) {
    this.isStandard = isStandard;
    return this;
  }

  /**
   * is the supplier part of the official good food suppliers ?
   * @return isStandard
  **/
  @ApiModelProperty(value = "is the supplier part of the official good food suppliers ?")


  public Boolean isIsStandard() {
    return isStandard;
  }

  public void setIsStandard(Boolean isStandard) {
    this.isStandard = isStandard;
  }

  public Supplier phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * the supplier phone number
   * @return phone
  **/
  @ApiModelProperty(required = true, value = "the supplier phone number")
  @NotNull


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Supplier email(String email) {
    this.email = email;
    return this;
  }

  /**
   * the supplier email
   * @return email
  **/
  @ApiModelProperty(required = true, value = "the supplier email")
  @NotNull


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Supplier ingredients(List<Ingredient> ingredients) {
    this.ingredients = ingredients;
    return this;
  }

  public Supplier addIngredientsItem(Ingredient ingredientsItem) {
    this.ingredients.add(ingredientsItem);
    return this;
  }

  /**
   * the ingredients provided by the supplier
   * @return ingredients
  **/
  @ApiModelProperty(required = true, value = "the ingredients provided by the supplier")
  @NotNull


  public List<Ingredient> getIngredients() {
    return ingredients;
  }

  public void setIngredients(List<Ingredient> ingredients) {
    this.ingredients = ingredients;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Supplier supplier = (Supplier) o;
    return Objects.equals(this.id, supplier.id) &&
        Objects.equals(this.name, supplier.name) &&
        Objects.equals(this.isStandard, supplier.isStandard) &&
        Objects.equals(this.phone, supplier.phone) &&
        Objects.equals(this.email, supplier.email) &&
        Objects.equals(this.ingredients, supplier.ingredients);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, isStandard, phone, email, ingredients);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Supplier {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    isStandard: ").append(toIndentedString(isStandard)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    ingredients: ").append(toIndentedString(ingredients)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


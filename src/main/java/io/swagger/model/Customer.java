package io.swagger.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.buisness.types.ObjectIdSerializer2;
import io.swagger.model.CustomerInformation;
import io.swagger.model.CustomerRGPD;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * customer model
 */
@ApiModel(description = "customer model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Customer   {
  @JsonProperty("id")
  @JsonSerialize(using = ObjectIdSerializer2.class)
  private ObjectId id = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("password")
  private String password = null;

  @JsonProperty("information")
  private CustomerInformation information = null;

  @JsonProperty("rgpd")
  private CustomerRGPD rgpd = null;

  public Customer id(ObjectId id) {
    this.id = id;
    return this;
  }

  /**
   * the id of the customer
   * @return id
  **/
  @ApiModelProperty(required = true, value = "the id of the customer")
  @NotNull


  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public Customer email(String email) {
    this.email = email;
    return this;
  }

  /**
   * the customer email, used as login
   * @return email
  **/
  @ApiModelProperty(required = true, value = "the customer email, used as login")
  @NotNull


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Customer password(String password) {
    this.password = password;
    return this;
  }

  /**
   * the customer password, encrypted
   * @return password
  **/
  @ApiModelProperty(required = true, value = "the customer password, encrypted")
  @NotNull


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Customer information(CustomerInformation information) {
    this.information = information;
    return this;
  }

  /**
   * the personal informations for this customer
   * @return information
  **/
  @ApiModelProperty(value = "the personal informations for this customer")

  @Valid

  public CustomerInformation getInformation() {
    return information;
  }

  public void setInformation(CustomerInformation information) {
    this.information = information;
  }

  public Customer rgpd(CustomerRGPD rgpd) {
    this.rgpd = rgpd;
    return this;
  }

  /**
   * the customer rgpd consents
   * @return rgpd
  **/
  @ApiModelProperty(value = "the customer rgpd consents")

  @Valid

  public CustomerRGPD getRgpd() {
    return rgpd;
  }

  public void setRgpd(CustomerRGPD rgpd) {
    this.rgpd = rgpd;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(this.id, customer.id) &&
        Objects.equals(this.email, customer.email) &&
        Objects.equals(this.password, customer.password) &&
        Objects.equals(this.information, customer.information) &&
        Objects.equals(this.rgpd, customer.rgpd);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, password, information, rgpd);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Customer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    information: ").append(toIndentedString(information)).append("\n");
    sb.append("    rgpd: ").append(toIndentedString(rgpd)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  public Document toDocument()
  {
	  DateFormat simpleDateFormat= new SimpleDateFormat("dd/MM/yyyy");
	
	  Document doc = new Document();
	  Document customerInformationDoc = new Document();
	  Document customerRgpdDoc = new Document();
	  System.out.println("ingredientApiImpl::customerToDoc customer = " + toString());
	  doc.put("email",getEmail());
	  doc.put("password",getPassword());
	  customerInformationDoc.put("firstName", getInformation().getFirstName());
	  customerInformationDoc.put("lastName", getInformation().getLastName());
	  customerInformationDoc.put("phone", getInformation().getPhone());
	  System.out.println("ingredientApiImpl::customerToDoc customer.getInformation().getBirthDate() = " + getInformation().getBirthDate().toString());
		customerInformationDoc.put("birthDate", simpleDateFormat.format(getInformation().getBirthDate()));
	  doc.put("information",customerInformationDoc);
	  customerRgpdDoc.put("hasConsentForDB", getRgpd().isHasConsentForDB());
	  customerRgpdDoc.put("dbConsentDate", simpleDateFormat.format(getRgpd().getDbConsentDate()));
	  customerRgpdDoc.put("hasConsentForCookies", getRgpd().isHasConsentForCookies());
	  customerRgpdDoc.put("cookieConsentDate", simpleDateFormat.format(getRgpd().getCookieConsentDate()));
	  doc.put("rgpd", customerRgpdDoc);
	  System.out.println("ingredientApiImpl::customerToDoc doc = " + doc.toString());
	  return doc;
  }
}


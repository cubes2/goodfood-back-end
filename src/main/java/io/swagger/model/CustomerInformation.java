package io.swagger.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.bson.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * customer information model
 */
@ApiModel(description = "customer information model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class CustomerInformation   {
  @JsonProperty("firstName")
  private String firstName = null;

  @JsonProperty("lastName")
  private String lastName = null;

  @JsonProperty("phone")
  private String phone = null;

  @DateTimeFormat(pattern="dd/MM/yyyy")
  @JsonProperty("birthDate")
  @JsonFormat(pattern="dd/MM/yyyy")
  private Date birthDate = null;
  
  public CustomerInformation(Document customerInfosDoc)
  {
	  System.out.println("CustomerInformation::CustomerInformation customerInfosDoc = " + customerInfosDoc.toString());
	  DateFormat simpleDateFormat= new SimpleDateFormat("dd/MM/yyyy");
	  
	  try 
	  {
		setBirthDate(simpleDateFormat.parse( customerInfosDoc.getString("birthDate")));
		setFirstName((String) customerInfosDoc.get("firstName"));
		setLastName((String) customerInfosDoc.get("lastName"));
		setPhone((String) customerInfosDoc.get("phone"));
	  } 
	  catch (ParseException e) 
	  {
		// TODO Auto-generated catch block
		  System.out.println("CustomerInformation::CustomerInformation ERROR = ");
	  }
	  
  }

  public CustomerInformation firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * customer firstname
   * @return firstName
  **/
  @ApiModelProperty(value = "customer firstname")


  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public CustomerInformation lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * customer lastname
   * @return lastName
  **/
  @ApiModelProperty(value = "customer lastname")


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public CustomerInformation phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * customer phone
   * @return phone
  **/
  @ApiModelProperty(required = true, value = "customer phone")
  @NotNull


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public CustomerInformation birthDate(Date birthDate) {
    this.birthDate = birthDate;
    return this;
  }

  /**
   * customer birthdate
   * @return birthDate
  **/
  @ApiModelProperty(value = "customer birthdate")

  @Valid

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomerInformation customerInformation = (CustomerInformation) o;
    return Objects.equals(this.firstName, customerInformation.firstName) &&
        Objects.equals(this.lastName, customerInformation.lastName) &&
        Objects.equals(this.phone, customerInformation.phone) &&
        Objects.equals(this.birthDate, customerInformation.birthDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, phone, birthDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomerInformation {\n");
    
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


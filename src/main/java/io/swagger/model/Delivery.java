package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * order delivery related informations
 */
@ApiModel(description = "order delivery related informations")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")


public class Delivery   {
  /**
   * either take-away or deliver
   */
  public enum TypeEnum {
    TAKE_AWAY("take-away"),
    
    DELIVER("deliver");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("type")
  private TypeEnum type = null;

  @JsonProperty("readyTime")
  private OffsetDateTime readyTime = null;

  @JsonProperty("arrivalTime")
  private OffsetDateTime arrivalTime = null;

  public Delivery type(TypeEnum type) {
    this.type = type;
    return this;
  }

  /**
   * either take-away or deliver
   * @return type
  **/
  @ApiModelProperty(value = "either take-away or deliver")


  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Delivery readyTime(OffsetDateTime readyTime) {
    this.readyTime = readyTime;
    return this;
  }

  /**
   * the time the order is ready to be delivered
   * @return readyTime
  **/
  @ApiModelProperty(value = "the time the order is ready to be delivered")

  @Valid

  public OffsetDateTime getReadyTime() {
    return readyTime;
  }

  public void setReadyTime(OffsetDateTime readyTime) {
    this.readyTime = readyTime;
  }

  public Delivery arrivalTime(OffsetDateTime arrivalTime) {
    this.arrivalTime = arrivalTime;
    return this;
  }

  /**
   * the estimated time of arrival of the order
   * @return arrivalTime
  **/
  @ApiModelProperty(value = "the estimated time of arrival of the order")

  @Valid

  public OffsetDateTime getArrivalTime() {
    return arrivalTime;
  }

  public void setArrivalTime(OffsetDateTime arrivalTime) {
    this.arrivalTime = arrivalTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Delivery delivery = (Delivery) o;
    return Objects.equals(this.type, delivery.type) &&
        Objects.equals(this.readyTime, delivery.readyTime) &&
        Objects.equals(this.arrivalTime, delivery.arrivalTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, readyTime, arrivalTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Delivery {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    readyTime: ").append(toIndentedString(readyTime)).append("\n");
    sb.append("    arrivalTime: ").append(toIndentedString(arrivalTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


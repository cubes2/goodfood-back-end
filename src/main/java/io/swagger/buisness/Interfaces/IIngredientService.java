package io.swagger.buisness.Interfaces;

import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import io.swagger.model.Ingredient;

public interface IIngredientService extends IService<Ingredient> {
	public List<Ingredient> GetAll();
	public Ingredient GetById(ObjectId id);
	public boolean Update(ObjectId id, Document object);
	public boolean Create(Document object);
	public boolean Delete(ObjectId id);
}

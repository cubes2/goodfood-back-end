package io.swagger.buisness.Interfaces;

import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

public interface IRestaurantService<Restaurant> extends IService<Restaurant> {
	public List<Restaurant> GetAll();
	public Restaurant GetById(ObjectId id);
	public boolean Update(ObjectId id, Document object);
	public boolean Create(Document object);
	public boolean Delete(ObjectId id);
}

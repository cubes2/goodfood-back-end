package io.swagger.buisness.Interfaces;

import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

public interface IService<T> {
	
	public List<T> GetAll();
	public T GetById(ObjectId id);
	public boolean Update(ObjectId id, Document object);
	public boolean Create(Document object);
	public boolean Delete(ObjectId id);

}

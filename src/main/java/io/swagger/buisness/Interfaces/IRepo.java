package io.swagger.buisness.Interfaces;

import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

public interface IRepo<T> {
	public List<T> GetAll();
	public T GetById(ObjectId id);
	public boolean Update(ObjectId id, T object);
	public boolean Create(T object);
	public boolean Delete(ObjectId id);
	public int getItemCount();

}

package io.swagger.buisness.Interfaces;

import java.util.List;

import io.swagger.myMongoDB;

public abstract class ABaseRepo<T> implements IRepo<T> {
	protected List<T> itemList;
	protected String tableName;
	protected myMongoDB db = new myMongoDB();
	public ABaseRepo(String table) {
		tableName = table;
	}
	public int getItemCount()
	{
		return itemList.size();
	}

}

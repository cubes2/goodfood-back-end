package io.swagger.buisness.Interfaces;

import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import io.swagger.model.Recipe;

public interface IRecipeService extends IService<Recipe> {
	public List<Recipe> GetAll();
	public Recipe GetById(ObjectId id);
	public boolean Update(ObjectId id, Document object);
	public boolean Create(Document object);
	public boolean Delete(ObjectId id);
}

package io.swagger.buisness.services.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import io.swagger.myMongoDB;
import io.swagger.Utils.ItemToDocument;
import io.swagger.buisness.Interfaces.ABaseRepo;
import io.swagger.buisness.Interfaces.ICustomerService;
import io.swagger.buisness.Interfaces.IRepo;
import io.swagger.model.Customer;
import io.swagger.model.CustomerInformation;
import io.swagger.model.CustomerRGPD;
import io.swagger.repo.CustomerRepo;

public class CustomerService implements ICustomerService<Customer> {

	private CryptoService myCryptoService;
	private IRepo<Customer> customerRepo;
	public CustomerService(IRepo<Customer> baseRepo) {
		// TODO Auto-generated constructor stub
		myCryptoService = new CryptoService(ThreadLocalRandom.current().nextInt(4, 31));
		customerRepo = baseRepo;
	}

	@Override
	public List<Customer> GetAll() {
		
		return customerRepo.GetAll();
	}

	@Override
	public Customer GetById(ObjectId id) {
		return customerRepo.GetById(id);
	}

	@Override
	public boolean Update(ObjectId id, Document object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Create(Document object) {
		boolean success = false;
		try
		{
			boolean isExist = false;
			object.replace("password",myCryptoService.hash((String) object.get("password")));
			Customer toCreate = ItemToDocument.documentToCustomer(object);
			List<Customer> existingCustomers = GetAll();
			for(Customer customer : existingCustomers)
			{
				if(customer.getEmail().equals(toCreate.getEmail()))
				{
					isExist = true;
				}
			}
			if(!isExist && customerRepo.Create(toCreate))
			{
				success = true;
			}
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

	@Override
	public boolean Delete(ObjectId id) {
		// TODO Auto-generated method stub
		return false;
	}

}

package io.swagger.buisness.services.impl;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

import io.swagger.myMongoDB;
import io.swagger.buisness.Interfaces.IRestaurantService;
import io.swagger.buisness.types.CellProduct;
import io.swagger.buisness.types.Person;
import io.swagger.model.Address;
import io.swagger.model.Customer;
import io.swagger.model.CustomerInformation;
import io.swagger.model.CustomerRGPD;
import io.swagger.model.Restaurant;

public class RestaurantService implements IRestaurantService<Restaurant> {

	public RestaurantService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Restaurant> GetAll() {
		List<Restaurant> all = new ArrayList<Restaurant>();
    	myMongoDB db = new myMongoDB();
    	System.out.println("RestaurantService::getAllRestaurantes myMongoDB created");
    	MongoCollection<Document> restaurants = db.GetDocumentBySellection("Restaurants");
    	System.out.println("RestaurantService::getAllRestaurantes MongoCollection<Restaurant> recovered");
    	MongoCursor<Document> restaurantsCursor = restaurants.find().iterator(); 
    	System.out.println("RestaurantService::getAllRestaurantes MongoCursor<Customer> init");
        try 
        { 
            while (restaurantsCursor.hasNext()) 
            { 
            	Document currentDoc = restaurantsCursor.next();
                System.out.println("RestaurantService::getAllRestaurantes currentDoc = "+currentDoc.toString());
                all.add(documentToRestaurant(currentDoc));
            } 
        }
        finally 
        { 
        	restaurantsCursor.close(); 
        } 
		return all;
	}

	@Override
	public Restaurant GetById(ObjectId id) {
		Restaurant restaurantToReturn = null;
		System.out.println("RestaurantService::GetById serched id is = " + id.toString());
		List<Restaurant> allRestaurants = new ArrayList<Restaurant>();
		allRestaurants = GetAll();
		System.out.println("RestaurantService::GetById allCustomers.size() = " + allRestaurants.size());
		for(Restaurant restaurant : allRestaurants)
		{
			System.out.println("RestaurantService::GetById customer.getId() = " + restaurant.getId().toString());
			if(restaurant.getId().equals(id))
			{
				restaurantToReturn = restaurant;
			}
		}
		if (restaurantToReturn != null) 
		{
			System.out.println("RestaurantService::GetById customerToReturn = " + restaurantToReturn.toString());	
		}
		
		return restaurantToReturn;
	}

	@Override
	public boolean Update(ObjectId id, Document object) {
		
		
		Restaurant restaurantToReturn = new Restaurant();
		restaurantToReturn = GetById(id);
		if(restaurantToReturn != null)
		{
			myMongoDB db = new myMongoDB();
			MongoCollection<Document> collection = db.GetDocumentBySellection("Restaurants");
			System.out.println("RestaurantService::Update restaurantToReturn.toDocument() = " + restaurantToReturn.toDocument().toString());
			System.out.println("RestaurantService::Update object = " + object.toString());
			Document testDocument = new Document();
			testDocument.put("$set", object);
			Document testDocument2 = new Document();
			testDocument2.put("_id", restaurantToReturn.getId());
			System.out.println("RestaurantService::Update testDocument2 = " + testDocument2.toString());
			System.out.println("RestaurantService::Update testDocument = " + testDocument.toString());
			collection.updateOne(testDocument2, testDocument);
			return true;
			
		}
		
		return false;
	}

	@Override
	public boolean Create(Document object) {
		System.out.println("RestaurantService::Create object = " + object.toString());
		boolean success = false;
		myMongoDB db = new myMongoDB();
		db.CreateModel("Restaurants",documentToRestaurant(object).toDocument());
		success = true;
		return success;
	}

	@Override
	public boolean Delete(ObjectId id) {
		// TODO Auto-generated method stub
		return false;
	}
	private Restaurant documentToRestaurant(Document doc)
	{
		  
		  
			
		System.out.println("RestaurantService::documentToRestaurant doc = " + doc.toString());
		DateFormat simpleDateFormat= new SimpleDateFormat("HH:mm");
		DateFormat simpleDateFormat2= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Restaurant restaurantToReturn = new Restaurant();
		restaurantToReturn.setId(doc.getObjectId("_id"));
		try {
			List<String> closeDateTimeDoc =(List<String>) doc.get("closeDateTime");
			List<Date> closeDateTimeList = new ArrayList<Date>();
			for(String closeDateTimeString : closeDateTimeDoc)
			{
					closeDateTimeList.add(simpleDateFormat.parse(closeDateTimeString));
			}
			restaurantToReturn.setCloseDateTime(closeDateTimeList);
			
		} catch (Exception e) {
				List<Date> closeDateTimeList=(List<Date>) doc.get("closeDateTime");
				restaurantToReturn.setCloseDateTime(closeDateTimeList);
		}
		
		
		
		restaurantToReturn.setName(doc.getString("name"));
		
		
		try {
			List<String> openDateTimeDoc =(List<String>) doc.get("openDateTime");
			List<Date> openDateTimeList = new ArrayList<Date>();
			for(String openDateTimeString : openDateTimeDoc)
			{
					openDateTimeList.add(simpleDateFormat.parse(openDateTimeString));
			}
			restaurantToReturn.setOpenDateTime(openDateTimeList);
			
		} catch (Exception e) {
				List<Date> openDateTimeList=(List<Date>) doc.get("openDateTime");
				restaurantToReturn.setOpenDateTime(openDateTimeList);
		}
		
		Document ownerDoc = new Document((Map<String, Object>) doc.get("owner"));
		restaurantToReturn.setOwner(new Person(ownerDoc));
		restaurantToReturn.setPhone(doc.getString("phone"));
		List<Object> recipeDoc = (List<Object>) doc.get("recipes"); //or in documents
		System.out.println("RestaurantService::documentToRestaurant recipeDoc = " + recipeDoc.toString());
		System.out.println("RestaurantService::documentToRestaurant recipeDoc.getClass().getSimpleName() = " + recipeDoc.getClass().getSimpleName());
		List<Document> listOfDocRecipeDocuments = new ArrayList<Document>();
		for(Object recipeObject : recipeDoc)
		{
			listOfDocRecipeDocuments.add(new Document((Map<String, Object>) recipeObject));
		}
		System.out.println("RestaurantService::documentToRestaurant listOfDocRecipeDocuments = " + listOfDocRecipeDocuments.toString());
		restaurantToReturn.setRecipesByDocuments(listOfDocRecipeDocuments);
		try {
			List<String> suppliersIds = doc.getList("suppliers", String.class);
			List<ObjectId> suppliersObjectsIds = new ArrayList<ObjectId>();
			for(String suppliersId: suppliersIds)
			{
				suppliersObjectsIds.add(new ObjectId(suppliersId));
			}
			restaurantToReturn.setSuppliers(suppliersObjectsIds);
		} catch (Exception e) {
			try {
				List<ObjectId> suppliersObjectsIds = doc.getList("suppliers", ObjectId.class);
				restaurantToReturn.setSuppliers(suppliersObjectsIds);
			} catch (Exception e1) {
				// TODO: handle exception
			}
		}
		
		
		Document addressDoc = new Document((Map<String, Object>) doc.get("address"));
		//recover address
		restaurantToReturn.setAddress(new Address(addressDoc));
		
		
		System.out.println("RestaurantService::documentToRestaurant restaurantToReturn = " + restaurantToReturn.toString());
		
		
		return restaurantToReturn;
	}

}

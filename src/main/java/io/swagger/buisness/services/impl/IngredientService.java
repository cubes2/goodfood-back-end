package io.swagger.buisness.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import io.swagger.myMongoDB;
import io.swagger.buisness.Interfaces.IIngredientService;
import io.swagger.buisness.types.Person;
import io.swagger.model.Address;
import io.swagger.model.Ingredient;
import io.swagger.model.Restaurant;

public class IngredientService implements IIngredientService {

	public IngredientService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Ingredient> GetAll() {
		List<Ingredient> all = new ArrayList<Ingredient>();
    	myMongoDB db = new myMongoDB();
    	System.out.println("IngredientService::getAllIngredients myMongoDB created");
    	MongoCollection<Document> ingredients = db.GetDocumentBySellection("Ingredients");
    	System.out.println("IngredientService::getAllIngredients MongoCollection<Ingredient> recovered");
    	MongoCursor<Document> ingredientsCursor = ingredients.find().iterator(); 
    	System.out.println("IngredientService::getAllIngredients MongoCursor<Ingredient> init");
        try 
        { 
            while (ingredientsCursor.hasNext()) 
            { 
            	Document currentDoc = ingredientsCursor.next();
                System.out.println("IngredientService::getAllIngredients currentDoc = "+currentDoc.toString());
                all.add(documentToIngredient(currentDoc));
            } 
        }
        finally 
        { 
        	ingredientsCursor.close(); 
        } 
		return all;
	}

	@Override
	public Ingredient GetById(ObjectId id) {
		Ingredient ingredientToReturn = null;
		System.out.println("IngredientService::GetById serched id is = " + id.toString());
		List<Ingredient> allIngredients = new ArrayList<Ingredient>();
		allIngredients = GetAll();
		System.out.println("IngredientService::GetById allCustomers.size() = " + allIngredients.size());
		for(Ingredient ingredient : allIngredients)
		{
			System.out.println("IngredientService::GetById customer.getId() = " + ingredient.getId().toString());
			if(ingredient.getId().equals(id))
			{
				ingredientToReturn = ingredient;
			}
		}
		if(ingredientToReturn != null)
		{
			System.out.println("IngredientService::GetById customerToReturn = " + ingredientToReturn.toString());
		}
		
		return ingredientToReturn;
	}

	@Override
	public boolean Update(ObjectId id, Document object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Create(Document object) {
		System.out.println("IngredientService::Create object = " + object.toString());
		boolean success = false;
		myMongoDB db = new myMongoDB();
		db.CreateModel("Ingredients",documentToIngredient(object).toDocument());
		success = true;
		return success;
	}

	@Override
	public boolean Delete(ObjectId id) {
		// TODO Auto-generated method stub
		return false;
	}
	private Ingredient documentToIngredient(Document doc)
	{
		  
		  
		Ingredient ingredientToReturn = new Ingredient();
		ingredientToReturn.setId(doc.getObjectId("_id"));
		ingredientToReturn.setAllergen(doc.getList("allergen", String.class));
		ingredientToReturn.setName(doc.getString("name"));
		System.out.println("IngredientService::documentToIngredient doc = " + doc.toString());
		return ingredientToReturn;
		
	}

}

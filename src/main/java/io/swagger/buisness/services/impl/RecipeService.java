package io.swagger.buisness.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import io.swagger.myMongoDB;
import io.swagger.buisness.Interfaces.IRecipeService;
import io.swagger.model.Recipe;

public class RecipeService implements IRecipeService {

	public RecipeService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Recipe> GetAll() {
		List<Recipe> all = new ArrayList<Recipe>();
    	myMongoDB db = new myMongoDB();
    	System.out.println("RecipeService::getAllRecipes myMongoDB created");
    	MongoCollection<Document> recipes = db.GetDocumentBySellection("Recipies");
    	System.out.println("RecipeService::getAllRecipes MongoCollection<Recipe> recovered");
    	MongoCursor<Document> recipesCursor = recipes.find().iterator(); 
    	System.out.println("RecipeService::getAllRecipes MongoCursor<Recipe> init");
        try 
        { 
            while (recipesCursor.hasNext()) 
            { 
            	Document currentDoc = recipesCursor.next();
                System.out.println("RecipeService::getAllRecipes currentDoc = "+currentDoc.toString());
                all.add(documentToRecipe(currentDoc));
            } 
        }
        finally 
        { 
        	recipesCursor.close(); 
        } 
		return all;
	}

	@Override
	public Recipe GetById(ObjectId id) {
		Recipe recipeToReturn = null;
		System.out.println("RecipeService::GetById serched id is = " + id.toString());
		List<Recipe> allRecipes = new ArrayList<Recipe>();
		allRecipes = GetAll();
		System.out.println("RecipeService::GetById allRecipes.size() = " + allRecipes.size());
		for(Recipe recipe : allRecipes)
		{
			System.out.println("RecipeService::GetById Recipe.getId() = " + recipe.getId().toString());
			if(recipe.getId().equals(id))
			{
				recipeToReturn = recipe;
			}
		}
		if(recipeToReturn != null)
		{
			System.out.println("RecipeService::GetById customerToReturn = " + recipeToReturn.toString());
		}
		
		return recipeToReturn;
	}

	@Override
	public boolean Update(ObjectId id, Document object) {
		Recipe recipeToReturn = new Recipe();
		recipeToReturn = GetById(id);
		if(recipeToReturn != null)
		{
			myMongoDB db = new myMongoDB();
			MongoCollection<Document> collection = db.GetDocumentBySellection("Recipies");
			System.out.println("RecipeService::Update recipeToReturn.toDocument() = " + recipeToReturn.toDocument().toString());
			System.out.println("RecipeService::Update object = " + object.toString());
			Document testDocument = new Document();
			testDocument.put("$set", object);
			Document testDocument2 = new Document();
			testDocument2.put("_id", recipeToReturn.getId());
			System.out.println("RecipeService::Update testDocument2 = " + testDocument2.toString());
			System.out.println("RecipeService::Update testDocument = " + testDocument.toString());
			collection.updateOne(testDocument2, testDocument);
			return true;
			
		}
		
		return false;
	}

	@Override
	public boolean Create(Document object) {
		System.out.println("RecipeService::Create object = " + object.toString());
		boolean success = false;
		myMongoDB db = new myMongoDB();
		db.CreateModel("Recipies",documentToRecipe(object).toDocument());
		success = true;
		return success;
	}

	@Override
	public boolean Delete(ObjectId id) {
		// TODO Auto-generated method stub
		return false;
	}
	private Recipe documentToRecipe (Document doc)
	{
		Recipe RecipeToReturn = new Recipe();
		RecipeToReturn.setId(doc.getObjectId("_id"));
		List<Object> ingredientsObjects = (List<Object>) doc.get("ingredients"); //or in documents
		List<Document> listOfDocingredientDocuments = new ArrayList<Document>();
		for(Object ingredientObject : ingredientsObjects)
		{
			listOfDocingredientDocuments.add(new Document((Map<String, Object>) ingredientObject));
		}
		RecipeToReturn.setIngredientsByDocuments(listOfDocingredientDocuments);
		RecipeToReturn.setIsStandard(doc.getBoolean("isStandard"));
		RecipeToReturn.setName(doc.getString("name"));
		RecipeToReturn.setOwner(doc.getString("owner"));
		RecipeToReturn.setPhoto(doc.getList("photo", String.class));
		RecipeToReturn.setStandardPrice(doc.get("standardPrice",Number.class).doubleValue());
		return RecipeToReturn;
	}

}

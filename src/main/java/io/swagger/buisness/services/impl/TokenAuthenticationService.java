package io.swagger.buisness.services.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

public class TokenAuthenticationService 
{
    
    static final long EXPIRATIONTIME = 3600000; // une heure
    
    static final String SECRET = "ThisIsASecret";
    
    static final String TOKEN_PREFIX = "Bearer";
    
    static final String HEADER_STRING = "Authorization";

    public static void addAuthentication(HttpServletResponse res, String username) 
    {
        String JWT = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime tokenValidUntil = today.plusSeconds(EXPIRATIONTIME/1000);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String strDate = tokenValidUntil.format(dateFormat);  
        String token =  "{\"token\": \"" + JWT + "\",\"validUntil\": \"" + strDate +"\"}";
        try 
        {
			res.getWriter().write(token);
		} 
        catch (IOException e) 
        {
        	System.out.println("TokenAuthenticationService::addAuthentication write token exception");
			e.printStackTrace();
		}
    }

    public static Authentication getAuthentication(HttpServletRequest request) 
    {
        String token = request.getHeader(HEADER_STRING);
        if (token != null)
        {
        	String user = new String();
	    	try
	    	{
	    		// parse the token.
	    		user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
	                    .getSubject();
			}
	        catch (ExpiredJwtException e)
	        {
	            System.out.println(" Token expired ");
	            return null;
	        }
	        catch (SignatureException e)
	        {
	        	System.out.println("SignatureException");
	        	return null;
	        }
	        catch(Exception e)
	        {
	            System.out.println(" Some other exception in JWT parsing ");
	            return null;
	        }
            Collection<? extends GrantedAuthority> emptyCollection = new ArrayList<GrantedAuthority>();
            emptyCollection = Collections.emptyList();
            return user != null ? new UsernamePasswordAuthenticationToken(user, null, emptyCollection) : null;
        }
        return null;
    }
    
}

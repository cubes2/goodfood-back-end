package io.swagger.buisness.types;



import org.bson.Document;

public class Person {

	public Person(Document doc) {
		System.out.println("Person::Person doc = " + doc.toString());
		setEmail(doc.getString("email"));
		setFirstName(doc.getString("firstName"));
		setLastName(doc.getString("lastName"));
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Document toDocument()
	{
		Document DocumentToReturn = new Document();
		DocumentToReturn.put("email", this.email);
		DocumentToReturn.put("firstName", this.firstName);
		DocumentToReturn.put("lastName", this.lastName);
		return DocumentToReturn;
		
	}
	private String email;
	private String firstName;
	private String lastName;

}

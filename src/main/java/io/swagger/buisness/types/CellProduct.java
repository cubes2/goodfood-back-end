package io.swagger.buisness.types;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.Document;
import org.bson.types.ObjectId;

public class CellProduct {

	public CellProduct(Document doc) {
		try {
			  setId(doc.getObjectId("id"));
		} catch (Exception e) {
			setId(new ObjectId(doc.getString("id")));
			// TODO: handle exception
		}
		setPrice(doc.get("price",Number.class).doubleValue());
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public Document toDocument()
	{
		Document DocumentToReturn = new Document();
		DocumentToReturn.put("id", this.id);
		DocumentToReturn.put("price", this.price);
		return DocumentToReturn;
		
	}
	private double price;
	@Id
	@JsonSerialize(using = ObjectIdSerializer2.class)
	private ObjectId id;
	

}

package io.swagger.buisness.types;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class StoreProduct {

	public StoreProduct(Document doc) {
		try {
			  setId(doc.getObjectId("id"));
		} catch (Exception e) {
			setId(new ObjectId(doc.getString("id")));
			// TODO: handle exception
		}
		setQuantity(doc.get("quantity",Number.class).intValue());
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public Document toDocument()
	{
		Document DocumentToReturn = new Document();
		DocumentToReturn.put("id", this.id);
		DocumentToReturn.put("quantity", this.quantity);
		return DocumentToReturn;
		
	}
	private int quantity;
	@Id
	@JsonSerialize(using = ObjectIdSerializer2.class)
	private ObjectId id;

}

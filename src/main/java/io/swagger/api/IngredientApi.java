/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.25).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.Ingredient;
import io.swagger.model.Recipe;
import io.swagger.myMongoDB;
import io.swagger.annotations.*;

import org.bson.Document;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.client.MongoCollection;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Validated
@Api(value = "ingredient", description = "the ingredient API")
@RequestMapping(value = "")
public interface IngredientApi {

    @ApiOperation(value = "Create an ingredient", nickname = "createIngredient", notes = "", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:content", description = "restaurant/recipe/ingredient/discount")
            })
    }, tags={ "ingredient", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/ingredient",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> createIngredient(@ApiParam(value = "Ingredient" ,required=true )  @Valid @RequestBody Document body);


    @ApiOperation(value = "Delete ingredient", nickname = "deleteIngredient", notes = "", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:content", description = "restaurant/recipe/ingredient/discount")
            })
    }, tags={ "ingredient", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Ingredient not found") })
    @RequestMapping(value = "/ingredient/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteIngredient(@ApiParam(value = "The ingredient that needs to be deleted",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns all recipe providing this ingredient", nickname = "getAllIngredientRecipes", notes = "Returns a json array of recipe", response = Recipe.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:content", description = "restaurant/recipe/ingredient/discount")
            }),
        @Authorization(value = "app_auth", scopes = {
            @AuthorizationScope(scope = "read:content", description = "restaurant/recipe/ingredient/discount")
            })
    }, tags={ "ingredient", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Recipe.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Ingredient not found") })
    @RequestMapping(value = "/ingredient/{uid}/recipe",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Recipe> getAllIngredientRecipes(@ApiParam(value = "ingredient ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns all ingredients", nickname = "getAllIngredients", notes = "Returns a json array of ingredients", response = Ingredient.class, responseContainer = "List", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:content", description = "restaurant/recipe/ingredient/discount")
            }),
        @Authorization(value = "app_auth", scopes = {
            @AuthorizationScope(scope = "read:content", description = "restaurant/recipe/ingredient/discount")
            })
    }, tags={ "ingredient", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Ingredient.class, responseContainer = "List"),
        @ApiResponse(code = 204, message = "No Content") })
    @RequestMapping(value = "/ingredient",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Ingredient>> getAllIngredients();


    @ApiOperation(value = "Returns a specific ingredient", nickname = "getIngredient", notes = "Returns a json ingredient", response = Ingredient.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:content", description = "restaurant/recipe/ingredient/discount")
            }),
        @Authorization(value = "app_auth", scopes = {
            @AuthorizationScope(scope = "read:content", description = "restaurant/recipe/ingredient/discount")
            })
    }, tags={ "ingredient", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Ingredient.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Ingredient not found") })
    @RequestMapping(value = "/ingredient/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Ingredient> getIngredient(@ApiParam(value = "ingredient ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Update an ingredient", nickname = "updateIngredient", notes = "Update an existing ingredient", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:content", description = "restaurant/recipe/ingredient/discount")
            })
    }, tags={ "ingredient", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ingredient supplied"),
        @ApiResponse(code = 404, message = "Ingredient not found") })
    @RequestMapping(value = "/ingredient/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    ResponseEntity<Void> updateIngredient(@ApiParam(value = "ingredient that need to be updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Updated Ingredient" ,required=true )  @Valid @RequestBody Document body);

}

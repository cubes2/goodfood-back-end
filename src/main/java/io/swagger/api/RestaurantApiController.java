package io.swagger.api;

import io.swagger.model.Customer;
import io.swagger.model.Discount;
import io.swagger.model.Feedback;
import io.swagger.model.Order;
import io.swagger.model.Purchase;
import io.swagger.model.Recipe;
import io.swagger.model.Restaurant;
import io.swagger.model.Supplier;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.apiImplement.CustomerApiImpl;
import io.swagger.apiImplement.RestaurantAPIImpl;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Controller
public class RestaurantApiController implements RestaurantApi {

    private static final Logger log = LoggerFactory.getLogger(RestaurantApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public RestaurantApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> createRestaurant(@ApiParam(value = "Restaurant" ,required=true )  @Valid @RequestBody Document body) {
    	String accept = request.getHeader("Accept");
        System.out.println("CustomerApiController::createRestaurant Restaurant = "+ body.toString());
        RestaurantAPIImpl restaurantApi = new RestaurantAPIImpl();
        restaurantApi.createRestaurant(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteRestaurant(@ApiParam(value = "The restaurant that needs to be deleted",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Discount>> getAllRestaurantDiscounts(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Discount>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Discount>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Discount>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Order> getAllRestaurantOrders(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Order>(objectMapper.readValue("{\"empty\": false}", Order.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Order>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Order>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Recipe>> getAllRestaurantRecipes(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Recipe>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Recipe>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Recipe>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Supplier>> getAllRestaurantSuppliers(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Supplier>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Supplier>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Supplier>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Restaurant>> getAllRestaurants() {
        String accept = request.getHeader("Accept");
        System.out.println("RestaurantApiController::getAllRestaurants entering");
        if (accept != null && accept.contains("application/json")) {
        	System.out.println("RestaurantApiController::getAllRestaurants application/json is ok");
        	RestaurantAPIImpl restaurantApi = new RestaurantAPIImpl();
        	return restaurantApi.getAllRestaurants();
        }

        return new ResponseEntity<List<Restaurant>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Restaurant> getRestaurant(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	RestaurantAPIImpl restaurantApi = new RestaurantAPIImpl();
        	return restaurantApi.getRestaurant(uid);
    }

    return new ResponseEntity<Restaurant>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Restaurant>> getRestaurantByGps(@ApiParam(value = "GPS location",required=true) @PathVariable("gps") String gps) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Restaurant>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Restaurant>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Restaurant>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Feedback> getRestaurantFeedbacks(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Feedback>(objectMapper.readValue("{\"empty\": false}", Feedback.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Feedback>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Feedback>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Purchase> getRestaurantPurchases(@ApiParam(value = "restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Purchase>(objectMapper.readValue("{\"empty\": false}", Purchase.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Purchase>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Purchase>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateRestaurant(@ApiParam(value = "restaurant that need to be updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Updated Restaurant" ,required=true )  @Valid @RequestBody Document body) {
        String accept = request.getHeader("Accept");
        System.out.println("CustomerApiController::createCustomer Customer = "+ body.toString());
        RestaurantAPIImpl restaurantApi = new RestaurantAPIImpl();
        restaurantApi.updateRestaurant(uid,body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}

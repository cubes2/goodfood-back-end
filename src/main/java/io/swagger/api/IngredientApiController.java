package io.swagger.api;

import io.swagger.model.Ingredient;
import io.swagger.model.Recipe;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.apiImplement.RestaurantAPIImpl;
import io.swagger.apiImplement.ingredientApiImpl;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Controller
public class IngredientApiController implements IngredientApi {

    private static final Logger log = LoggerFactory.getLogger(IngredientApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public IngredientApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> createIngredient(@ApiParam(value = "Ingredient" ,required=true )  @Valid @RequestBody Document body) {
        String accept = request.getHeader("Accept");
        System.out.println("IngredientApiController::createIngredient ingredient = "+ body.toString());
        ingredientApiImpl ingredientApi = new ingredientApiImpl();
        ingredientApi.createIngredient(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteIngredient(@ApiParam(value = "The ingredient that needs to be deleted",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Recipe> getAllIngredientRecipes(@ApiParam(value = "ingredient ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Recipe>(objectMapper.readValue("{\"empty\": false}", Recipe.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Recipe>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Recipe>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Ingredient>> getAllIngredients() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            	ingredientApiImpl ingredientAPI = new ingredientApiImpl();
            	return ingredientAPI.getAllIngredients();
        }

        return new ResponseEntity<List<Ingredient>>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Ingredient> getIngredient(@ApiParam(value = "ingredient ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	ingredientApiImpl ingredientAPI = new ingredientApiImpl();
        	return ingredientAPI.getIngredient(uid);
        }

        return new ResponseEntity<Ingredient>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateIngredient(@ApiParam(value = "ingredient that need to be updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Updated Ingredient" ,required=true )  @Valid @RequestBody Document body) {
        String accept = request.getHeader("Accept"); 
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}

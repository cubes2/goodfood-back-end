package io.swagger.api;

import io.swagger.model.Address;
import io.swagger.model.Complaint;
import io.swagger.model.Customer;
import io.swagger.model.CustomerInformation;
import io.swagger.model.CustomerRGPD;
import io.swagger.model.Feedback;
import io.swagger.model.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.apiImplement.CustomerApiImpl;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Controller
public class CustomerApiController implements CustomerApi {

    private static final Logger log = LoggerFactory.getLogger(CustomerApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public CustomerApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> createComplaint(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Customer Complaint" ,required=true )  @Valid @RequestBody Complaint body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> createCustomer(@ApiParam(value = "Customer" ,required=true )  @Valid @RequestBody Document body) {
        String accept = request.getHeader("Accept");
        System.out.println("CustomerApiController::createCustomer Customer = "+ body.toString());
        CustomerApiImpl customerApi = new CustomerApiImpl();
    	customerApi.createCustomer(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> createCustomerAddress(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Customer Address" ,required=true )  @Valid @RequestBody Address body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> createCustomerInformation(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Customer Information" ,required=true )  @Valid @RequestBody CustomerInformation body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> createCustomerOrder(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Customer Order" ,required=true )  @Valid @RequestBody Order body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> createCustomerRGPD(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Customer RGPD Information" ,required=true )  @Valid @RequestBody CustomerRGPD body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> createFeedback(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Customer Feedback" ,required=true )  @Valid @RequestBody Feedback body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteCustomer(@ApiParam(value = "The customer that needs to be deleted",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteCustomerInformation(@ApiParam(value = "customer that need to have its information deleted",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Customer>> getAllCustomers() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	CustomerApiImpl customerApi = new CustomerApiImpl();
        	return customerApi.getAllCustomers();
        }

        return new ResponseEntity<List<Customer>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Customer> getCustomer(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            	CustomerApiImpl customerApi = new CustomerApiImpl();
            	return customerApi.getCustomer(uid);
        }

        return new ResponseEntity<Customer>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Address> getCustomerAddresses(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Address>(objectMapper.readValue("{\"empty\": false}", Address.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Address>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Address>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Complaint> getCustomerComplaints(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Complaint>(objectMapper.readValue("{\"empty\": false}", Complaint.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Complaint>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Complaint>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Feedback> getCustomerFeedbacks(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Feedback>(objectMapper.readValue("{\"empty\": false}", Feedback.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Feedback>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Feedback>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<CustomerInformation> getCustomerInformation(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<CustomerInformation>(objectMapper.readValue("{\"empty\": false}", CustomerInformation.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<CustomerInformation>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<CustomerInformation>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Order> getCustomerOrders(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Order>(objectMapper.readValue("{\"empty\": false}", Order.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Order>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Order>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<CustomerRGPD> getCustomerRGPD(@ApiParam(value = "customer ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<CustomerRGPD>(objectMapper.readValue("{\"empty\": false}", CustomerRGPD.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<CustomerRGPD>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<CustomerRGPD>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateCustomer(@ApiParam(value = "customer that need to be updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Updated Customer" ,required=true )  @Valid @RequestBody Customer body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateCustomerInformation(@ApiParam(value = "customer that need to have its information updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Concern Customer" ,required=true )  @Valid @RequestBody CustomerInformation body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateCustomerRGPD(@ApiParam(value = "customer that need to have its rgpd information updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Concern Customer" ,required=true )  @Valid @RequestBody CustomerRGPD body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}

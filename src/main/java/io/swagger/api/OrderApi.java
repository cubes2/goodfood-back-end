/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.25).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.Address;
import io.swagger.model.Delivery;
import io.swagger.model.Order;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Validated
@Api(value = "order", description = "the order API")
@RequestMapping(value = "")
public interface OrderApi {

    @ApiOperation(value = "Update order status to approved", nickname = "approveOrder", notes = "Allow a restaurant to approve an order", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found") })
    @RequestMapping(value = "/order/{uid}/approve",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> approveOrder(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Update order status to delivered", nickname = "deliverOrder", notes = "Allow a restaurant to confirm an order delivery", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found") })
    @RequestMapping(value = "/order/{uid}/deliver",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> deliverOrder(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns all orders", nickname = "getAllOrders", notes = "Returns a json array of orders", response = Order.class, responseContainer = "List", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Order.class, responseContainer = "List"),
        @ApiResponse(code = 204, message = "No Content") })
    @RequestMapping(value = "/order",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Order>> getAllOrders();


    @ApiOperation(value = "Returns a specific order", nickname = "getOrder", notes = "Returns a json order", response = Order.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:transaction", description = "order/complaint")
            }),
        @Authorization(value = "app_auth", scopes = {
            @AuthorizationScope(scope = "read:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Order.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found") })
    @RequestMapping(value = "/order/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Order> getOrder(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns a specific order address", nickname = "getOrderAddress", notes = "Returns a json order address", response = Address.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:transaction", description = "order/complaint")
            }),
        @Authorization(value = "app_auth", scopes = {
            @AuthorizationScope(scope = "read:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Address.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found") })
    @RequestMapping(value = "/order/{uid}/address",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Address> getOrderAddress(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns a specific order delivery informations", nickname = "getOrderDelivery", notes = "Returns a json delivery informations", response = Delivery.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Delivery.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found") })
    @RequestMapping(value = "/order/{uid}/delivery",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Delivery> getOrderDelivery(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Update order with payment information", nickname = "payOrder", notes = "Allow a customer to pay an order", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:transaction", description = "order/complaint")
            }),
        @Authorization(value = "app_auth", scopes = {
            @AuthorizationScope(scope = "write:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/order/{uid}/pay",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> payOrder(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Payment Reference" ,required=true )  @Valid @RequestBody String body);


    @ApiOperation(value = "Update order status to provided", nickname = "provideOrder", notes = "Allow a restaurant to confirm an order readyness", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:transaction", description = "order/complaint")
            })
    }, tags={ "order", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Order not found") })
    @RequestMapping(value = "/order/{uid}/provide",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> provideOrder(@ApiParam(value = "order ID",required=true) @PathVariable("uid") String uid);

}

/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.25).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.Admin;
import io.swagger.model.Permission;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Validated
@Api(value = "admin", description = "the admin API")
@RequestMapping(value = "")
public interface AdminApi {

    @ApiOperation(value = "Create an admin", nickname = "createAdmin", notes = "", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:admin", description = "admin/supplier/purchase")
            })
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/admin",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> createAdmin(@ApiParam(value = "Admin" ,required=true )  @Valid @RequestBody Admin body);


    @ApiOperation(value = "Delete an admin", nickname = "deleteAdmin", notes = "", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:admin", description = "admin/supplier/purchase")
            })
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Admin not found") })
    @RequestMapping(value = "/admin/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteAdmin(@ApiParam(value = "The admin that needs to be deleted",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns a specific admin", nickname = "getAdmin", notes = "Returns a json admin", response = Admin.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:admin", description = "admin/supplier/purchase")
            })
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Admin.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Admin not found") })
    @RequestMapping(value = "/admin/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Admin> getAdmin(@ApiParam(value = "admin ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns a specific admin permissions", nickname = "getAdminPermissions", notes = "Returns a string array", response = Permission.class, authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:admin", description = "admin/supplier/purchase")
            })
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Permission.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Admin not found") })
    @RequestMapping(value = "/admin/{uid}/permission",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Permission> getAdminPermissions(@ApiParam(value = "admin ID",required=true) @PathVariable("uid") String uid);


    @ApiOperation(value = "Returns all admins", nickname = "getAllAdmins", notes = "Returns a json array of admins", response = Admin.class, responseContainer = "List", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "read:admin", description = "admin/supplier/purchase")
            })
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Admin.class, responseContainer = "List"),
        @ApiResponse(code = 204, message = "No Content") })
    @RequestMapping(value = "/admin",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Admin>> getAllAdmins();


    @ApiOperation(value = "Update an admin", nickname = "updateAdmin", notes = "Update an existing admin", authorizations = {
        @Authorization(value = "admin_auth", scopes = {
            @AuthorizationScope(scope = "write:admin", description = "admin/supplier/purchase")
            })
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid admin supplied"),
        @ApiResponse(code = 404, message = "Admin not found") })
    @RequestMapping(value = "/admin/{uid}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    ResponseEntity<Void> updateAdmin(@ApiParam(value = "admin that need to be updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Updated Admin" ,required=true )  @Valid @RequestBody Admin body);

}

package io.swagger.api;

import io.swagger.model.Discount;
import io.swagger.model.Feedback;
import io.swagger.model.Ingredient;
import io.swagger.model.Recipe;
import io.swagger.model.Restaurant;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.apiImplement.RecipeApiImpl;
import io.swagger.apiImplement.RestaurantAPIImpl;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-29T16:10:28.297Z")

@Controller
public class RecipeApiController implements RecipeApi {

    private static final Logger log = LoggerFactory.getLogger(RecipeApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public RecipeApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> addRecipe(@ApiParam(value = "recipe group ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> createRecipe(@ApiParam(value = "Recipe" ,required=true )  @Valid @RequestBody Document body) {
        String accept = request.getHeader("Accept");
        System.out.println("RecipeApiController::createRecipe Recipe = "+ body.toString());
        RecipeApiImpl recipeApi = new RecipeApiImpl();
        recipeApi.createRecipe(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteRecipe(@ApiParam(value = "The recipe that needs to be deleted",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Discount>> getAllRecipeActiveDiscounts(@ApiParam(value = "recipe ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Discount>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Discount>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Discount>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Ingredient> getAllRecipeIngredients(@ApiParam(value = "recipe ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Ingredient>(objectMapper.readValue("{\"empty\": false}", Ingredient.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Ingredient>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Ingredient>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Restaurant> getAllRecipeRestaurants(@ApiParam(value = "recipe ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Restaurant>(objectMapper.readValue("{\"empty\": false}", Restaurant.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Restaurant>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Restaurant>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Recipe>> getAllRecipes() {
        String accept = request.getHeader("Accept");
        System.out.println("RecipeApiController::getAllRecipes entering");
        if (accept != null && accept.contains("application/json")) {
        	System.out.println("RecipeApiController::getAllRecipes application/json is ok");
        	RecipeApiImpl recipeApi = new RecipeApiImpl();
        	return recipeApi.getAllRecipes();
        }

        return new ResponseEntity<List<Recipe>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Recipe> getRecipe(@ApiParam(value = "recipe ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	RecipeApiImpl recipeApi = new RecipeApiImpl();
        	return recipeApi.getRecipe(uid);
        }

        return new ResponseEntity<Recipe>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Feedback> getRecipeFeedbacks(@ApiParam(value = "recipe ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Feedback>(objectMapper.readValue("{\"empty\": false}", Feedback.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Feedback>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Feedback>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> removeRecipe(@ApiParam(value = "recipe group ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> standardiseRecipe(@ApiParam(value = "recipe restaurant ID",required=true) @PathVariable("uid") String uid) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateRecipe(@ApiParam(value = "recipe that need to be updated",required=true) @PathVariable("uid") String uid,@ApiParam(value = "Updated Recipe" ,required=true )  @Valid @RequestBody Document body) {
        String accept = request.getHeader("Accept");
        System.out.println("RecipeApiController::updateRecipe Recipe = "+ body.toString());
        RecipeApiImpl recipeApi = new RecipeApiImpl();
        recipeApi.updateRecipe(uid,body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}

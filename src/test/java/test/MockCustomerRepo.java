package test;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;

import io.swagger.model.Customer;
import io.swagger.repo.CustomerRepo;

public class MockCustomerRepo extends CustomerRepo {

	public MockCustomerRepo() {
		// TODO Auto-generated constructor stub
	}
	public List<Customer> GetAll() 
	{
		return itemList;
	}
	public Customer GetById(ObjectId id)
	{
		for (Customer customer : itemList)
		{
			if(customer.getId().toString().equals(id.toString()))
			{
				return customer;
			}
		}
		return null;
	}
	public boolean Update(ObjectId id, Customer object)
	{
		//not implemented for the moment
		return false;
	}
	public boolean Create(Customer object)
	{
		object.setId(new ObjectId());
		itemList.add(object);
		return true;
	}
	public boolean Delete(ObjectId id)
	{
		boolean toReturn = false;
		int found = itemList.indexOf(GetById(id));
		if(found != -1)
		{
			itemList.remove(found);
			toReturn = true;
		}
		return toReturn;
	}

}

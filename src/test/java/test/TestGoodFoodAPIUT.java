package test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import io.swagger.buisness.services.impl.CustomerService;
import io.swagger.model.Customer;
import org.junit.jupiter.api.Test;

public class TestGoodFoodAPIUT {

	
	@Test
	public void TestCustomerService()
	{
		//delete Customers table content
		CustomerService myCustomerService = new CustomerService(new MockCustomerRepo());
		/*if(TestCustomerServiceGetById(myCustomerService,"623f003b7b5ef41130a29354"))
		{
			//test customers values
			Customer myCustomer = TestCustomerServiceGetByIdAndReturnIt(myCustomerService,"623f003b7b5ef41130a29354");
			String customerRefString = "Customer {\r\n"
			+ "    id: 6202285a81f5654068879383\r\n"
			+ "    email: toto@gmail.com\r\n"
			+ "    password: passwordBidon\r\n"
			+ "    information: class CustomerInformation {\r\n"
			+ "        firstName: toto\r\n"
			+ "        lastName: toto2\r\n"
			+ "        phone: 0123456789\r\n"
			+ "        birthDate: Mon Feb 07 00:00:00 CET 2022\r\n"
			+ "    }\r\n"
			+ "    rgpd: class CustomerRGPD {\r\n"
			+ "        hasConsentForDB: true\r\n"
			+ "        dbConsentDate: Mon Feb 07 00:00:00 CET 2022\r\n"
			+ "        hasConsentForCookies: true\r\n"
			+ "        cookieConsentDate: Mon Feb 07 00:00:00 CET 2022\r\n"
			+ "    }\r\n"
			+ "}";
			assertEquals(myCustomer.toString(),customerRefString);
		}
		else 
		{
			assertTrue(false);
		}*/
		String customerToCreateString = "{\"email\": \"toto2@gmail.com\",\"password\": \"passwordBidon\",\"information\": {\"firstName\": \"toto\",\"lastName\": \"toto2\",\"phone\": \"0123456789\",\"birthDate\": \"07/02/2022\"},\"rgpd\": {\"hasConsentForDB\": true,\"dbConsentDate\": \"07/02/2022\",\"hasConsentForCookies\": true,\"cookieConsentDate\": \"07/02/2022\"}}";
		Document customer1 = new Document(Document.parse(customerToCreateString));
		TestCustomerServiceCreate(myCustomerService,customer1);
		
		assertEquals(TestCustomerServiceGetAll(myCustomerService),1);
	}
	int TestCustomerServiceGetAll(CustomerService myCustomerService)
	{
		List<Customer> myCustomerList = myCustomerService.GetAll();
		//veriffy list of customer
		return myCustomerList.size();
	}
	boolean TestCustomerServiceGetById(CustomerService myCustomerService, String customerId)
	{
		ObjectId myCustomerId = new ObjectId(customerId);
		Customer myCustomer = myCustomerService.GetById(myCustomerId);
		Customer myCustomerReferenceCustomer = new Customer();
		//fill myCustomerReferenceCustomer
		if(myCustomer != null)
		{
			return true;
		}
		return false;
	}
	Customer TestCustomerServiceGetByIdAndReturnIt(CustomerService myCustomerService, String customerId)
	{
		ObjectId myCustomerId = new ObjectId(customerId);
		Customer myCustomer = myCustomerService.GetById(myCustomerId);
		Customer myCustomerReferenceCustomer = new Customer();
		//fill myCustomerReferenceCustomer
		if(myCustomer != null)
		{
			if(myCustomerReferenceCustomer.equals(myCustomer))
			{
				return myCustomer;
			}
			
		}
		return null;
	}
	boolean TestCustomerServiceCreate(CustomerService myCustomerService,Document toCreate)
	{
		if(myCustomerService.Create(toCreate))
		{
			return true;
		}
		return false;
	}
	

}

#!/bin/bash
JAVA_VER=$(sshpass -e ssh -tt -o StrictHostKeyChecking=no debian@5.196.74.128 java -version | grep Environment)
if [ -z "$JAVA_VER" ]
then
    echo "java is not installed"
    sshpass -e ssh -tt -o StrictHostKeyChecking=no debian@5.196.74.128 sudo apt install openjdk-17-jdk
    sshpass -e ssh -tt -o StrictHostKeyChecking=no debian@5.196.74.128 sudo apt install openjdk-17-jre
else
    echo "java is installed"
fi
exit 0
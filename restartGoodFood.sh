

#!/bin/bash
GOOD_FOOD=$(sshpass -e ssh -tt -o StrictHostKeyChecking=no debian@5.196.74.128 ps ax | grep -c swagger-spring-1.0.0.jar)
if [ $GOOD_FOOD=1 ]; 
then
    echo "good food is not started"
else
    echo "good food is started"
    sshpass -e ssh -tt -o StrictHostKeyChecking=no debian@5.196.74.128 sudo pkill -9 -f swagger-spring-1.0.0.jar
fi
exit 0